<?php

include 'model/Emprunt.php';
include 'resource/function/dateFunction.php';
include 'resource/function/priceFunction.php';
include 'resource/function/mailFunction.php';

$arrBilan = select(11,0);
for ($i = 0; $i < sizeof($arrBilan); $i++ ) {
    $arrBilan[$i]['dateEmprunt'] = dateFormatDisplay($arrBilan [$i]['dateEmprunt']);
    $arrBilan[$i]['dette'] = priceFormatDisplay($arrBilan [$i]['dette']);
}

if ( isset($_POST['sendMail'])) {
    for ($i = 0; $i < sizeof($arrBilan); $i++ ) {
        sendMail($arrBilan[$i]['email'],$arrBilan[$i]['nomAdherent'], $arrBilan[$i]['titre'], $arrBilan[$i]['retard']);
    }
}