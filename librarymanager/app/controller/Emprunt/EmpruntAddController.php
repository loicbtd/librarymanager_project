<?php

include 'model/Emprunt.php';
include 'resource/function/dateFunction.php';

$isAdded = false;

date_default_timezone_set('Europe/Paris');

// getting the list of adherent
$arrAdherent = select(12,0);
// getting the list of available exemplaire:
$arrExemplaireDispo = select(5,0);
// getting the number of available exemplaire:
$nbExemplaireDispo = (int)select(7,0)[0]['nbExemplaireDispo'];

if ( isset($_POST['idAdherent']) ) {
    $queryParameter['idAdherent'] = htmlentities($_POST['idAdherent']);
    if ($queryParameter['idAdherent'] != "0") {
        // getting selected adherent informations
        $currentAdherent = select(2, $queryParameter)[0];
        // getting the number of emprunt of the selected adherent
        $nbEmprunt = (int)select(3, $queryParameter)[0]['nbEmpruntNotReturned'];
        // getting the list of emprunt of the selected adherent
        $arrEmprunt = select(4, $queryParameter);
        for ($i = 0; $i < sizeof($arrEmprunt); $i++ ) {
            $arrEmprunt[$i]['dateEmprunt'] = dateFormatDisplay($arrEmprunt[$i]['dateEmprunt']);
        }

        $hasPaid = ( select(8, $queryParameter)[0]['flagRetard'] == "0" );
    }
}

if ( isset($_POST['noExemplaire']) ) {
    $queryParameter['noExemplaire'] = htmlentities($_POST['noExemplaire']);
    if ($queryParameter['noExemplaire'] != "0"){
        $currentExemplaire = select(6, $queryParameter)[0];
    }
}

if ( isset($_POST['dateEmprunt']) ) {
    $queryParameter['dateEmprunt'] = htmlentities($_POST['dateEmprunt']);
    if ( testDate($queryParameter['dateEmprunt']) != 0 ) {
        $error['dateEmprunt'] = setDateError( testDate($queryParameter['dateEmprunt']) );
    }
}

if ( isset($currentAdherent) && isset($currentExemplaire) && empty($error) && isset($_POST['validate'] ) ) {
    $queryParameter['dateEmprunt'] = dateFormatSql($queryParameter['dateEmprunt']);
    insert(1,$queryParameter);
    $isAdded=true;
    // update emprunt list of the current adherent to show the inserted value + nbEmprunt
    $arrEmprunt = select(4, $queryParameter);
    for ($i = 0; $i < sizeof($arrEmprunt); $i++ ) {
        $arrEmprunt[$i]['dateEmprunt'] = dateFormatDisplay($arrEmprunt[$i]['dateEmprunt']);
    }
    $nbEmprunt = (int)select(3, $queryParameter)[0]['nbEmpruntNotReturned'];
}