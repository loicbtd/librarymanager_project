<?php

include 'model/Emprunt.php';
include 'resource/function/dateFunction.php';
include 'resource/function/textFunction.php';

$arrAdherent = select(1,0);

if ( isset($_POST['arrEmpruntToDel']) && isset($_POST['validate']) ) {
    foreach ( $_POST['arrEmpruntToDel'] as $empruntToDel) {

        $strParsed = parseEmpruntKeys($empruntToDel);
        $delParameter['idAdherent'] = $strParsed[0];
        $delParameter['dateEmprunt'] = dateFormatSql($strParsed[1]);
        $delParameter['noExemplaire'] = $strParsed[2];

        delete(1,$delParameter);
    }
}

if ( isset($_POST['idAdherent']) ) {
    $queryParameter['idAdherent'] = htmlentities($_POST['idAdherent']);
    if ($queryParameter['idAdherent'] != "0") {
        $arrEmprunt = select(10,$queryParameter);
        // getting selected adherent informations
        $currentAdherent = select(2, $queryParameter)[0];
    }
    else {
        $arrEmprunt = select(9,0);
    }
    for ($i = 0; $i < sizeof($arrEmprunt); $i++ ) {
        $arrEmprunt[$i]['dateEmprunt'] = dateFormatDisplay($arrEmprunt[$i]['dateEmprunt']);
        $arrEmprunt[$i]['dateRendu'] = dateFormatDisplay($arrEmprunt[$i]['dateRendu']);
    }
}
else {
    $arrEmprunt = select(9,0);
    for ($i = 0; $i < sizeof($arrEmprunt); $i++ ) {
        $arrEmprunt[$i]['dateEmprunt'] = dateFormatDisplay($arrEmprunt[$i]['dateEmprunt']);
        $arrEmprunt[$i]['dateRendu'] = dateFormatDisplay($arrEmprunt[$i]['dateRendu']);
    }
}