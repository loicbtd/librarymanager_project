<?php

include 'model/Emprunt.php';
include 'resource/function/dateFunction.php';
include 'resource/function/textFunction.php';

$arrAdherent = select(14,0);

//  if adherent is set
if (isset($_POST['idAdherentCur'])) {
    $queryParameter['idAdherentCur'] = htmlentities($_POST['idAdherentCur']);
    if ($queryParameter['idAdherentCur'] != "0") {
        $arrEmprunt = select(4, $queryParameter);
        $currentAdherent = select(2, $queryParameter)[0];
    }
    else {
        $arrEmprunt = select(13, 0);
    }
}
else {
    $arrEmprunt = select(13, 0);
}
// set the good date format to display
for ($i = 0; $i < sizeof($arrEmprunt); $i++) {
    $arrEmprunt[$i]['dateEmprunt'] = dateFormatDisplay($arrEmprunt[$i]['dateEmprunt']);
}



// if user click on return
if(isset($_POST['validate'])) {
//    set queryParameter (+ control the entered date)
    $queryParameter['dateRendu'] = htmlentities($_POST['dateRendu']);
    if (testDate($queryParameter['dateRendu']) != 0) {
        $error['dateRendu'] = setDateError(testDate($queryParameter['dateRendu']));
    }
    $queryParameter['dateEmprunt'] = htmlentities($_POST['dateEmprunt']);
    $queryParameter['noExemplaire'] = htmlentities($_POST['noExemplaire']);
    $queryParameter['idAdherent'] = htmlentities($_POST['idAdherent']);

//    if date is ok
    if (empty($error)) {
        $queryParameter['dateRendu'] = dateFormatSql($queryParameter['dateRendu']);
        $queryParameter['dateEmprunt'] = dateFormatSql($queryParameter['dateEmprunt']);
        update(2, $queryParameter);

//        update the list of emprunt of the current adherent
        if ( isset($_POST['idAdherentCur']) ) {
            $arrEmprunt = select(4, $queryParameter);
        }
        else {
            $arrEmprunt = select(13, 0);
        }
        for ($i = 0; $i < sizeof($arrEmprunt); $i++) {
            $arrEmprunt[$i]['dateEmprunt'] = dateFormatDisplay($arrEmprunt[$i]['dateEmprunt']);
        }
    }
}
