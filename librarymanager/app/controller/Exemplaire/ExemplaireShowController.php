<?php

include 'model/Exemplaire.php';
include 'resource/function/dateFunction.php';

if ( isset($_POST['noOeuvre']) ) {
    $queryParameter['noOeuvre'] = htmlentities($_POST['noOeuvre']);
    $nbExemplaire = (int)select(3,$queryParameter)[0]['nbExemplaire'];
    $currentOeuvre = select(2,$queryParameter)[0];
    $currentOeuvre['dateParution'] = dateFormatDisplay($currentOeuvre['dateParution']);
    $queryAnswer = select(1,$queryParameter);
    for ($i = 0; $i < sizeof($queryAnswer); $i++ ) {
        $queryAnswer[$i]['dateAchat'] = dateFormatDisplay($queryAnswer[$i]['dateAchat']);
    }
}

