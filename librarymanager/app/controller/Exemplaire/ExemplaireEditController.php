<?php

include 'model/Exemplaire.php';
include 'resource/function/dateFunction.php';
include 'resource/function/priceFunction.php';

$isModified=false;

if ( isset($_POST['noExemplaire']) ) {
    $queryParameter['noExemplaire'] = htmlentities($_POST['noExemplaire']);
}

if ( isset($_POST['noOeuvre']) ) {
    $queryParameter['noOeuvre'] = htmlentities($_POST['noOeuvre']);
    $currentOeuvre = select(2,$queryParameter)[0];
    $currentOeuvre['dateParution'] = dateFormatDisplay($currentOeuvre['dateParution']);
}

if ( isset($_POST['etat']) ) {
    $queryParameter['etat'] = htmlentities($_POST['etat']);
    if ( $queryParameter['etat'] == "0" ) {
        $error['etat'] = "l'état doit être renseigné";
    }
}

if ( isset($_POST['dateAchat']) ) {
    $queryParameter['dateAchat'] = htmlentities($_POST['dateAchat']);
    if ( testDate($queryParameter['dateAchat']) != 0 ) {
        $error['dateAchat'] = setDateError( testDate($queryParameter['dateAchat']) );
    }
}

if ( isset($_POST['prix']) ) {
    $queryParameter['prix'] = htmlentities($_POST['prix']);
    $queryParameter['prix'] = priceFormatDisplay($queryParameter['prix']);
    if ( testPrice($queryParameter['prix']) != 0 ) {
        $error['prix'] = setPriceError( testPrice($queryParameter['prix']) );
    }
}


if( isset($queryParameter['noOeuvre'])
    && isset($queryParameter['etat'])
    && isset($queryParameter['dateAchat'])
    && isset($queryParameter['prix'])
    && empty($error)
    && isset($_POST['confirmation'])) {
    $queryParameter['dateAchat'] = dateFormatSql($queryParameter['dateAchat']);
    $queryParameter['prix'] = priceFormatSql($queryParameter['prix']);
    update(1, $queryParameter);
    $isModified=true;
}

