<?php

include 'model/Exemplaire.php';

$isDeleted = false;

if (isset($_POST['origin'])) {
    $origin = htmlentities($_POST['origin']);
}

if (isset($_POST['noOeuvre'])) {
    // secure the var
    $queryParameter['noOeuvre'] = htmlentities($_POST['noOeuvre']);
}

// check if post var is set
if (isset($_POST['noExemplaire'])) {
    // secure the var
    $queryParameter['noExemplaire'] = htmlentities($_POST['noExemplaire']);
    // calculate the number of oeuvre of current auteur
    $nbEmprunt = (int)select(4, $queryParameter)[0]['nbEmprunt'];
}

// if everything is good, delete current auteur
if ( isset($queryParameter['noExemplaire']) && isset($_POST['confirmation']) ) {
    $queryAnswerDelete = delete(1, $queryParameter);
    $isDeleted = true;
}