<?php

include 'model/Auteur.php';
include 'resource/function/textFunction.php';

$isAdded=false;

if ( isset($_POST['nomAuteur']) ) {
    $queryParameter['nomAuteur'] = htmlentities($_POST['nomAuteur']);
    if ( !isNameValid($queryParameter['nomAuteur']) ) {
        $error['nomAuteur'] = setTextError(1);
    }
}

if ( isset($_POST['prenomAuteur']) ) {
    $queryParameter['prenomAuteur'] = htmlentities($_POST['prenomAuteur']);
    if ( !isNameValid($queryParameter['prenomAuteur']) ) {
        $error['prenomAuteur'] = setTextError(1);
    }
}

if( !empty($queryParameter) && empty($error) ) {
    insert(1, $queryParameter);
    $isAdded=true;
}
