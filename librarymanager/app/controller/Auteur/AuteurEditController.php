<?php

include 'model/Auteur.php';
include 'resource/function/textFunction.php';

$isModified=false;

if ( isset($_POST['idAuteur']) ) {
    $queryParameter['idAuteur'] = htmlentities($_POST['idAuteur']);
}

if ( isset($_POST['nomAuteur']) ) {
    $queryParameter['nomAuteur'] = htmlentities($_POST['nomAuteur']);
    if ( !isNameValid($queryParameter['nomAuteur']) ) {
        $error['nomAuteur'] = setTextError(1);
    }
}

if ( isset($_POST['prenomAuteur']) ) {
    $queryParameter['prenomAuteur'] = htmlentities($_POST['prenomAuteur']);
    if ( !isNameValid($queryParameter['prenomAuteur']) ) {
        $error['prenomAuteur'] = setTextError(1);
    }
}

if( !empty($queryParameter) && empty($error) && isset($_POST['validate']) ) {
    update(1, $queryParameter);
    $isModified=true;
}