<?php

include 'model/Auteur.php';

// while nothing is done
$isDeleted=false;

// check if post var is set
if ( isset($_POST['idAuteur']) ) {
    // secure the var
    $queryParameter['idAuteur'] = htmlentities($_POST['idAuteur']);
    // calculate the number of oeuvre of current auteur
    $nbOeuvre = (int)select(2,$queryParameter)[0]['nbOeuvre'];
    // if this auteur has 1 or + oeuvre, set an error
    if( !$nbOeuvre==0 ) {
        $error['nbOeuvre']=$nbOeuvre;
    }
}
else {
    // if not set, generate an error
    $error['idAuteur']="idAuteur error";
}

// if everything is good, delete current auteur
if( !empty($queryParameter) && empty($error) && isset($_POST['validate']) && $nbOeuvre==0 ) {
    $queryAnswerDelete=delete(1,$queryParameter);
    $isDeleted=true;
}


