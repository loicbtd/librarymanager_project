<?php

include 'model/Oeuvre.php';
include 'resource/function/dateFunction.php';
include 'resource/function/textFunction.php';

$isAdded=false;

$queryAnswer = select(2,0);

if ( isset($_POST['titre']) ) {
    $queryParameter['titre'] = htmlentities($_POST['titre']);
    if ( !isNameValid($queryParameter['titre']) ) {
        $error['titre'] = setTextError(1);
    }
}

if ( isset($_POST['dateParution']) ) {
    $queryParameter['dateParution'] = htmlentities($_POST['dateParution']);
    if ( testDate($queryParameter['dateParution']) != 0 ) {
        $error['dateParution'] = setDateError( testDate($queryParameter['dateParution']) );
    }
}

if ( isset($_POST['idAuteur']) ) {
    $queryParameter['idAuteur'] = htmlentities($_POST['idAuteur']);
    if ( $queryParameter['idAuteur'] == "0" ) {
        $error['idAuteur'] = "veuillez sélectionner un auteur";
    }
}

if( !empty($queryParameter) && empty($error) ) {
    $queryParameter['dateParution'] = dateFormatSql($queryParameter['dateParution']);
    insert(1, $queryParameter);
    $isAdded=true;
}