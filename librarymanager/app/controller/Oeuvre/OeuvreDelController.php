<?php

include 'model/Oeuvre.php';
include 'resource/function/dateFunction.php';

// while nothing is done
$isDeleted=false;

// check if post var is set
if ( isset($_POST['noOeuvre']) ) {
    // secure the var
    $queryParameter['noOeuvre'] = htmlentities($_POST['noOeuvre']);
    // calculate the number of oeuvre of current auteur
    $nbExemplaire = (int)select(3,$queryParameter)[0]['nbExemplaire'];
    // if this auteur has 1 or + oeuvre, set an error
    if( !$nbExemplaire==0 ) {
        $error['nbExemplaire']=$nbExemplaire;
    }
    $queryAnswer = select(4,$queryParameter);
    for ($i = 0; $i < sizeof($queryAnswer); $i++ ) {
        $queryAnswer[$i]['dateAchat'] = dateFormatDisplay($queryAnswer[$i]['dateAchat']);
    }
}
else {
    // if not set, generate an error
    $error['noOeuvre']="noOeuvre error";
}

// if everything is good, delete current auteur
if( !empty($queryParameter) && empty($error) && isset($_POST['validate']) && $nbExemplaire==0 ) {
    $queryAnswerDelete=delete(1,$queryParameter);
    $isDeleted=true;
}

