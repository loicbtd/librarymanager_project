<?php

include 'model/Adherent.php';

// while nothing is done
$isDeleted=false;

// check if post var is set
if ( isset($_POST['idAdherent']) ) {
    // secure the var
    $queryParameter['idAdherent'] = htmlentities($_POST['idAdherent']);
    // calculate the number of emprunts of current adherent
    $nbEmprunt = (int)select(2,$queryParameter)[0]['nbEmprunt'];
    // if this adherent has 1 or + emprunt, set an error
    if( !$nbEmprunt==0 ) {
        $error['nbEmprunt']=$nbEmprunt;
    }
}
else {
    // if not set, generate an error
    $error['idAdherent']="idAdherent error";
}

// if everything is good, delete current adherent
if( !empty($queryParameter) && empty($error) && isset($_POST['validate']) && $nbEmprunt==0 ) {
    $queryAnswerDelete=delete(1,$queryParameter);
    $isDeleted=true;
}


