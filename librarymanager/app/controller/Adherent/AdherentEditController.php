<?php

include 'model/Adherent.php';
include 'resource/function/textFunction.php';
include 'resource/function/dateFunction.php';

$isModified=false;

if ( isset($_POST['idAdherent']) ) {
    $queryParameter['idAdherent'] = htmlentities($_POST['idAdherent']);
}

if ( isset($_POST['nomAdherent']) ) {
    $queryParameter['nomAdherent'] = htmlentities($_POST['nomAdherent']);
    if ( !isNameValid($queryParameter['nomAdherent']) ) {
        $error['nomAdherent'] = setTextError("name");
    }
}

if ( isset($_POST['adresse']) ) {
    $queryParameter['adresse'] = htmlentities($_POST['adresse']);
    if ( !isNameValid($queryParameter['adresse']) ) {
        $error['adresse'] = setTextError("address");
    }
}
if ( isset($_POST['datePaiement']) ) {
    $queryParameter['datePaiement'] = htmlentities($_POST['datePaiement']);
    if ( testDate($queryParameter['datePaiement']) != 0 ) {
        $error['datePaiement'] = setDateError( testDate($queryParameter['datePaiement']) );
    }
}
if ( isset($_POST['email']) ) {
    $queryParameter['email'] = htmlentities($_POST['email']);
    if ( !isEmailValid($queryParameter['email']) ) {
        $error['email'] = setTextError("email");
    }
}

if( !empty($queryParameter) && empty($error) && isset($_POST['validate']) ) {
    $queryParameter['datePaiement'] = dateFormatSql($queryParameter['datePaiement']);
    update(1, $queryParameter);
    $isModified=true;
}