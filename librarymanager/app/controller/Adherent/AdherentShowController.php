<?php

include 'model/Adherent.php';
include 'resource/function/dateFunction.php';

$queryAnswer = select(1,0);

for ($i = 0; $i < sizeof($queryAnswer); $i++ ) {
    $queryAnswer[$i]['datePaiement'] = dateFormatDisplay($queryAnswer[$i]['datePaiement']);
    if(isset($queryAnswer[$i]['dateButoir'])){
        $queryAnswer[$i]['dateButoir'] = dateFormatDisplay($queryAnswer[$i]['dateButoir']);
    }

}