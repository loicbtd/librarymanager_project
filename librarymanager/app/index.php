<?php
session_start();
if ( isset($_POST['logout']) ) {
    session_destroy();
    header('Location:/');
}
?>

<?php include 'resource/include/head.php'; ?>

<?php //if ( isset($_SESSION['isLogged']) ): ?>
<?php //if ( $_SESSION['isLogged'] ): ?>
    <?php include 'resource/include/nav.php'; ?>
    <div class="container mt-3 mb-5">
        <?php
        if ( isset($_GET['page']) ) {
            switch($_GET['page']) {
                case "AdherentShow":
                    include 'view/Adherent/AdherentShow.php' ;
                    $path[0] = "Adhérents";
                    $path[1] = "Consultation";
                    break;

                case "AdherentAdd":
                    include 'view/Adherent/AdherentAdd.php';
                    $path[0] = "Adhérents";
                    $path[1] = "Ajout";
                    break;
                case "AdherentDel":
                    include 'view/Adherent/AdherentDel.php' ;
                    $path[0] = "Adhérents";
                    $path[1] = "Suppression";
                    break;
                case "AdherentEdit":
                    include 'view/Adherent/AdherentEdit.php' ;
                    $path[0] = "Adhérents";
                    $path[1] = "Modification";
                    break;

                case "AuteurAdd":
                    include 'view/Auteur/AuteurAdd.php' ;
                    $path[0] = "Auteurs";
                    $path[1] = "Ajout";
                    break;

                case "AuteurDel":
                    include 'view/Auteur/AuteurDel.php' ;
                    $path[0] = "Auteurs";
                    $path[1] = "Suppression";
                    break;

                case "AuteurShow":
                    include 'view/Auteur/AuteurShow.php' ;
                    $path[0] = "Auteurs";
                    $path[1] = "Consultation";
                    break;

                case "AuteurEdit":
                    include 'view/Auteur/AuteurEdit.php' ;
                    $path[0] = "Auteurs";
                    $path[1] = "Modification";
                    break;

                case "EmpruntAdd":
                    include 'view/Emprunt/EmpruntAdd.php' ;
                    $path[0] = "Emprunts";
                    $path[1] = "Ajout";
                    break;

                case "EmpruntBilan":
                    include 'view/Emprunt/EmpruntBilan.php' ;
                    $path[0] = "Emprunts";
                    $path[1] = "Retards";
                    break;

                case "EmpruntDel":
                    include 'view/Emprunt/EmpruntDel.php';
                    $path[0] = "Emprunts";
                    $path[1] = "Suivi";
                    break;

                case "EmpruntReturn":
                    include 'view/Emprunt/EmpruntReturn.php' ;
                    $path[0] = "Emprunts";
                    $path[1] = "Retours";
                    break;

                case "OeuvreShow":
                    include 'view/Oeuvre/OeuvreShow.php' ;
                    $path[0] = "Oeuvres";
                    $path[1] = "Consultation";
                    break;

                case "OeuvreAdd":
                    include 'view/Oeuvre/OeuvreAdd.php' ;
                    $path[0] = "Oeuvres";
                    $path[1] = "Ajout";
                    break;

                case "OeuvreDel":
                    include 'view/Oeuvre/OeuvreDel.php' ;
                    $path[0] = "Oeuvres";
                    $path[1] = "Suppression";
                    break;

                case "OeuvreEdit":
                    include 'view/Oeuvre/OeuvreEdit.php' ;
                    $path[0] = "Oeuvres";
                    $path[1] = "Modification";
                    break;

                case "ExemplaireShow":
                    include 'view/Exemplaire/ExemplaireShow.php' ;
                    $path[0] = "Exemplaires";
                    $path[1] = "Consultation";
                    break;

                case "ExemplaireAdd":
                    include 'view/Exemplaire/ExemplaireAdd.php' ;
                    $path[0] = "Exemplaires";
                    $path[1] = "Ajout";
                    break;

                case "ExemplaireDel":
                    include 'view/Exemplaire/ExemplaireDel.php' ;
                    $path[0] = "Exemplaires";
                    $path[1] = "Suppression";
                    break;

                case "ExemplaireEdit":
                    include 'view/Exemplaire/ExemplaireEdit.php' ;
                    $path[0] = "Exemplaires";
                    $path[1] = "Modification";
                    break;
            }
        }
        else {
            include 'view/Dashboard.php' ;
        }
        ?>
    </div>
    <?php include 'resource/include/footer.php'; ?>
<?php //else: ?>
<!--    --><?php //include 'resource/include/logPage.php'; ?>
<?php //endif; ?>
<?php //else: ?>
<!--    --><?php //include 'resource/include/logPage.php'; ?>
<?php //endif; ?>

<?php include 'resource/include/foot.php'; ?>
