<?php include 'controller/Oeuvre/OeuvreShowController.php' ?>

<div class="row">
    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <a href="/OeuvreAdd" class="btn btn-block btn-success my-1" tabindex="-1" role="button" aria-disabled="true"><i class="fas fa-plus"></i></a>
    </div>
    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <table class="table table-responsive-lg">
            <thead class="thead-dark">
            <tr class="text-center" >
                <th scope="col">Nom de l'auteur</th>
                <th scope="col">Titre</th>
                <th scope="col">Date de parution</th>
                <th scope="col">Total des exemplaire(s)</th>
                <th scope="col">Disponible(s)</th>
                <th scope="col">Exemplaire(s)</th>
                <th scope="col">Opérations</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($queryAnswer as $row): ?>
                <tr class="text-center">
                    <th scope="row"><?=$row['nomAuteur']; ?></th>
                    <td><?=$row['titre']; ?></td>
                    <td><?=$row['dateParution']; ?></td>
                    <td><?=$row['nbTotal']; ?></td>
                    <td><?=$row['nbDispo']; ?></td>
                    <td scope="col">
                        <form class="w-100" action="/ExemplaireShow" method="post">
                            <input type="hidden" class="form-control" name="noOeuvre" <?php  if(isset($row['noOeuvre'])):?> value="<?=$row['noOeuvre']; ?>"<?php endif; ?>>
                            <button type="submit" class="btn btn-sm btn-dark"><i class="fas fa-tasks"></i></button>
                        </form>
                    </td>
                    <td scope="col">
                        <div class="d-inline-flex">
                            <form class="w-50" action="/OeuvreEdit" method="post">
                                <input type="hidden" class="form-control" name="noOeuvre" <?php  if(isset($row['noOeuvre'])):?> value="<?=$row['noOeuvre']; ?>"<?php endif; ?>>
                                <input type="hidden" class="form-control" name="titre"<?php  if(isset($row['titre'])):?> value="<?=$row['titre']; ?>"<?php endif; ?>>
                                <input type="hidden" class="form-control" name="dateParution" <?php  if(isset($row['dateParution'])):?> value="<?=$row['dateParution']; ?>"<?php endif; ?>>
                                <input type="hidden" class="form-control" name="idAuteur" <?php  if(isset($row['idAuteur'])):?> value="<?=$row['idAuteur']; ?>"<?php endif; ?>>
                                <button type="submit" class="btn btn-sm btn-dark mx-1"><i class="fas fa-edit"></i></button>
                            </form>
                            <form class="w-50" action="/OeuvreDel" method="post">
                                <input type="hidden" class="form-control" name="noOeuvre" <?php  if(isset($row['noOeuvre'])):?> value="<?=$row['noOeuvre']; ?>"<?php endif; ?>>
                                <button type="submit" class="btn btn-sm btn-dark mx-1"><i class="fas fa-trash"></i></button>
                            </form>
                        </div>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>