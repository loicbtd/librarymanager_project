<?php include 'controller/Oeuvre/OeuvreAddController.php'; ?>

<div class="row">
<!--    TODO: display an alert if there is no auteur (impossible to add oeuvre)-->
    <?php if(!$isAdded): ?>
        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <form action="/OeuvreAdd" method="post">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label class="font-weight-bold" for="titre" >Titre</label>
                        <input type="text" class="form-control" name="titre" <?php  if(isset($queryParameter['titre'])):?> value="<?=$queryParameter['titre']; ?>"<?php endif; ?> placeholder="saisir un titre">
                        <?php if(isset($error['titre'])): ?>
                            <small class="form-text font-weight-bold text-danger"><?=$error['titre'] ?></small>
                        <?php endif; ?>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="font-weight-bold" for="dateParution">Date de parution</label>
                        <input type="text" class="form-control" name="dateParution" <?php  if(isset($queryParameter['dateParution'])):?> value="<?=$queryParameter['dateParution']; ?>"<?php endif; ?> placeholder="saisir une date">
                        <?php if(isset($error['dateParution'])): ?>
                            <small class="form-text font-weight-bold text-danger"><?=$error['dateParution'] ?></small>
                        <?php endif; ?>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="idAuteur">Auteur</label>
                        <select class="form-control" name="idAuteur">
                            <option value="0">sélectionner un auteur</option>
                            <?php foreach ($queryAnswer as $row): ?>
                            <option value="<?=$row['idAuteur']?>"
                                <?php if( isset($queryParameter['idAuteur']) && $queryParameter['idAuteur']==$row['idAuteur']): ?>
                                    selected
                                <?php endif; ?>
                            >
                                <?=$row['nomAuteur']?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                        <?php if(isset($error['idAuteur'])): ?>
                            <small class="form-text font-weight-bold text-danger"><?=$error['idAuteur'] ?></small>
                        <?php endif; ?>
                    </div>
                </div>
                <button type="submit" class="btn btn-dark m-1">Valider</button>
                <a href="/OeuvreShow" class="btn btn-light m-1" tabindex="-1" role="button" aria-disabled="true">Retour</a>
            </form>
        </div>
    <?php else: ?>
        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="alert alert-success" role="alert">
                <p><strong>Information:</strong> l'oeuvre a bien été ajoutée.</p>
                <a href="/OeuvreAdd" class="btn btn-success mx-1" tabindex="-1" role="button" aria-disabled="true">Nouveau</a>
                <a href="/OeuvreShow" class="btn btn-light mx-1" tabindex="-1" role="button" aria-disabled="true">Retour</a>
            </div>
        </div>
    <?php endif; ?>
</div>