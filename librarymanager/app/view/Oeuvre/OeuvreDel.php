<?php include 'controller/Oeuvre/OeuvreDelController.php'; ?>

<div class="row">
    <?php if( isset($error['nbExemplaire']) ): ?>
        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="alert alert-danger" role="alert">
                <?php if( $error['nbExemplaire'] > 1 ): ?>
                <p><strong>Erreur:</strong> impossible de supprimer cette oeuvre car les <strong><?=$error['nbExemplaire']?></strong> exemplaires ci-dessous lui font référence.</p>
                <?php else: ?>
                <p><strong>Erreur:</strong> impossible de supprimer cette oeuvre car l'exemplaire ci-dessous lui fait référence.</p>
                <?php endif; ?>
                <a href="/OeuvreShow" class="btn btn-danger m-1" tabindex="-1" role="button" aria-disabled="true">Retour</a>
            </div>
        </div>
        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <table class="table table-responsive-lg">
                <thead class="thead-dark">
                <tr class="text-center" >
                    <th scope="col">N°</th>
                    <th scope="col">État</th>
                    <th scope="col">Date d'achat</th>
                    <th scope="col">Prix</th>
                    <th scope="col">Opérations</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($queryAnswer as $row): ?>
                    <tr class="text-center">
                        <th scope="row"><?=$row['noExemplaire']; ?></th>
                        <td><?=$row['etat']; ?></td>
                        <td><?=$row['dateAchat']; ?></td>
                        <td><?=number_format((float)$row['prix'], 2, ",", " ")." €"?></td>
                        <td scope="col">
                            <div class="d-inline-flex">
                                <form class="w-50" action="/ExemplaireDel" method="post">
                                    <input type="hidden" class="form-control" name="noExemplaire" <?php  if(isset($row['noExemplaire'])):?> value="<?=$row['noExemplaire']; ?>"<?php endif; ?>>
                                    <input type="hidden" class="form-control" name="noOeuvre" <?php  if(isset($queryParameter['noOeuvre'])):?> value="<?=$queryParameter['noOeuvre']; ?>"<?php endif; ?>>
                                    <input type="hidden" class="form-control" name="origin" value="OeuvreDel">
                                    <button type="submit" class="btn btn-sm btn-dark mx-1"><i class="fas fa-trash"></i></button>
                                </form>
                            </div>
                        </td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
        </div>
    <?php elseif($isDeleted): ?>
        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="alert alert-success" role="alert">
                <p><strong>Information:</strong> l'oeuvre a bien été supprimée.</p>
                <a href="/OeuvreShow" class="btn btn-success m-1" tabindex="-1" role="button" aria-disabled="true">Retour</a>
            </div>
        </div>
    <?php else: ?>
        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="card">
                <div class="card-body">
                    <h2 class="card-title">Information</h2>
                    <p class="text-danger font-weight-bold">Cette action est irreversible.</p>
                    <p class="font-weight-bold">Voulez-vous vraiment supprimer cette oeuvre ? </p>
                    <form id="form-button" action="/OeuvreDel" method="post">
                        <input type="hidden" class="form-control" name="noOeuvre" <?php  if(isset($queryParameter['noOeuvre'])):?> value="<?=$queryParameter['noOeuvre']; ?>"<?php endif; ?>>
                        <input type="hidden" class="form-control" name="validate" value="y">
                        <button type="submit" class="btn btn btn-danger m-1">Supprimer</button>
                    </form>
                    <a href="/OeuvreShow" class="btn btn-dark m-1" tabindex="-1" role="button" aria-disabled="true">Retour</a>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>