<?php include 'controller/Auteur/AuteurShowController.php'; ?>

<div class="row">
    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <a href="/AuteurAdd" class="btn btn-block btn-success my-1" tabindex="-1" role="button" aria-disabled="true"><i class="fas fa-plus"></i></a>
    </div>
    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <table class="table table-responsive-sm">
            <thead class="thead-dark">
            <tr class="text-center" >
                <th scope="col">Nom</th>
                <th scope="col">Prénom</th>
                <th scope="col">Nombre d'oeuvres</th>
                <th scope="col">Opérations</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($queryAnswer as $row): ?>
                <tr class="text-center">
                    <th scope="row"><?=$row['nomAuteur']; ?></th>
                    <td><?=$row['prenomAuteur']; ?></td>
                    <td><?=$row['nbOeuvre']; ?></td>
                    <td scope="col">
                        <div class="d-inline-flex">
                            <form class="w-50" action="/AuteurEdit" method="post">
                                <input type="hidden" class="form-control" name="idAuteur" <?php  if(isset($row['idAuteur'])):?> value="<?=$row['idAuteur']; ?>"<?php endif; ?>>
                                <input type="hidden" class="form-control" name="nomAuteur"<?php  if(isset($row['nomAuteur'])):?> value="<?=$row['nomAuteur']; ?>"<?php endif; ?>>
                                <input type="hidden" class="form-control" name="prenomAuteur" <?php  if(isset($row['prenomAuteur'])):?> value="<?=$row['prenomAuteur']; ?>"<?php endif; ?>>
                                <button type="submit" class="btn btn-sm btn-dark mx-1"><i class="fas fa-edit"></i></button>
                            </form>
                            <form class="w-50" action="/AuteurDel" method="post">
                                <input type="hidden" class="form-control" name="idAuteur" <?php  if(isset($row['idAuteur'])):?> value="<?=$row['idAuteur']; ?>"<?php endif; ?>>
                                <button type="submit" class="btn btn-sm btn-dark mx-1"><i class="fas fa-trash"></i></button>
                            </form>
                        </div>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>