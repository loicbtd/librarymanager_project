<?php include 'controller/Auteur/AuteurAddController.php'; ?>

<div class="row">
    <?php if(!$isAdded): ?>
    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <form action="/AuteurAdd" method="post">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label class="font-weight-bold" for="nomAuteur" >Nom</label>
                    <input type="text" class="form-control" name="nomAuteur" <?php  if(isset($queryParameter['nomAuteur'])):?> value="<?=$queryParameter['nomAuteur']; ?>"<?php endif; ?> placeholder="saisir un nom">
                    <?php if(isset($error['nomAuteur'])): ?>
                        <small class="form-text font-weight-bold text-danger"><?=$error['nomAuteur'] ?></small>
                    <?php endif; ?>
                </div>
                <div class="form-group col-md-6">
                    <label class="font-weight-bold" for="prenomAuteur">Prénom</label>
                    <input type="text" class="form-control" name="prenomAuteur" <?php  if(isset($queryParameter['prenomAuteur'])):?> value="<?=$queryParameter['prenomAuteur']; ?>"<?php endif; ?> placeholder="saisir un prenom">
                    <?php if(isset($error['prenomAuteur'])): ?>
                        <small class="form-text font-weight-bold text-danger"><?=$error['prenomAuteur'] ?></small>
                    <?php endif; ?>
                </div>
            </div>
            <button type="submit" class="btn btn-dark m-1">Valider</button>
            <a href="/AuteurShow" class="btn btn-light m-1" tabindex="-1" role="button" aria-disabled="true">Retour</a>
        </form>
    </div>
    <?php else: ?>
    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="alert alert-success" role="alert">
            <p><strong>Information:</strong> l'auteur a bien été ajouté.</p>
            <a href="/AuteurAdd" class="btn btn-success mx-1" tabindex="-1" role="button" aria-disabled="true">Nouveau</a>
            <a href="/AuteurShow" class="btn btn-light mx-1" tabindex="-1" role="button" aria-disabled="true">Retour</a>
        </div>
    </div>
    <?php endif; ?>
</div>