<?php include 'controller/Auteur/AuteurDelController.php'; ?>

<div class="row">
    <?php if( isset($error['nbOeuvre']) ): ?>
    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="alert alert-danger" role="alert">
            <p><strong>Erreur:</strong> impossible de supprimer cet auteur car encore <strong><?=$error['nbOeuvre']?> oeuvre(s)</strong> lui font référence.</p>
            <a href="/AuteurShow" class="btn btn-danger m-1" tabindex="-1" role="button" aria-disabled="true">Retour</a>
        </div>
    </div>
    <?php elseif($isDeleted): ?>
    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="alert alert-success" role="alert">
            <p><strong>Information:</strong> l'auteur a bien été supprimé.</p>
            <a href="/AuteurShow" class="btn btn-success m-1" tabindex="-1" role="button" aria-disabled="true">Retour</a>
        </div>
    </div>
    <?php else: ?>
    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card">
            <div class="card-body">
                <h2 class="card-title">Information</h2>
                <p class="text-danger font-weight-bold">Cette action est irreversible.</p>
                <p class="font-weight-bold">Voulez-vous vraiment supprimer cet auteur ? </p>
                <form id="form-button" action="/AuteurDel" method="post">
                    <input type="hidden" class="form-control" name="idAuteur" <?php  if(isset($queryParameter['idAuteur'])):?> value="<?=$queryParameter['idAuteur']; ?>"<?php endif; ?>>
                    <input type="hidden" class="form-control" name="validate" value="y">
                    <button type="submit" class="btn btn btn-danger m-1">Supprimer</button>
                </form>
                <a href="/AuteurShow" class="btn btn-dark m-1" tabindex="-1" role="button" aria-disabled="true">Retour</a>
            </div>
        </div>
    </div>
    <?php endif; ?>
</div>