<?php include 'controller/Emprunt/EmpruntAddController.php'; ?>

<div class="row">
    <div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
<!--        BEGIN: display adherent infos-->
        <?php if ( !isset($currentAdherent['idAdherent'])): ?>
        <div class="card my-2">
            <div class="card-body">
                <form action="/EmpruntAdd" method="post">
                    <label for="idAdherent">Adhérent</label>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <select id="idAdherent" class="form-control" name="idAdherent" onchange="this.form.submit()">
                                    <option value="0">sélectionner un adhérent</option>
                                    <?php foreach ($arrAdherent as $row): ?>
                                        <option value="<?=$row['idAdherent']?>">
                                            <?=$row['nomAdherent']?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                </form>
            </div>
        </div>
        <?php else: ?>
        <div class="card my-2">
            <div class="card-body">
                <h6 class="card-title font-weight-bold">Adhérent</h6>
                <h5><span class="badge badge-dark">M./Mme <?=$currentAdherent['nomAdherent']?></span></h5>
                <a href="/EmpruntAdd" class="btn btn-sm btn-light">Changer</a>
            </div>
        </div>
        <?php endif; ?>
<!--       END:  Display adherent infos-->
<!--        BEIGN: display exemplaire infos-->
        <?php if ( !$nbExemplaireDispo == 0): ?>
        <?php if ( isset($currentAdherent['idAdherent']) && !$isAdded ): ?>
        <?php if ( !$hasPaid ): ?>
        <div class="alert alert-danger">
            <span><strong>Information:</strong> l'adhérent doit renouveler son abonnement pour emprunter.</span>
        </div>
        <?php elseif ( $nbEmprunt < 5 ): ?>
        <div class="card my-2">
            <div class="card-body">
                <h6 class="font-weight-bold">Ajout d'un emprunt</h6>
                <form action="/EmpruntAdd" method="post">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label class="font-weight-bold" for="noExemplaire">Exemplaire</label>
                            <select id="noExemplaire" class="form-control" name="noExemplaire" onchange="this.form.submit()">
                                <option value="0">sélectionner un exemplaire</option>
                                <?php foreach ($arrExemplaireDispo as $row): ?>
                                <option value="<?=$row['noExemplaire']?>"
                                    <?php if( isset($queryParameter['noExemplaire']) && $queryParameter['noExemplaire']==$row['noExemplaire']): ?>
                                    selected
                                    <?php endif; ?>>
                                    <?=$row['nomAuteur']?> - <?=$row['titre']?> n°<?=$row['noExemplaire']?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                            <input type="hidden" class="form-control" name="idAdherent" <?php  if(isset($currentAdherent['idAdherent'])):?> value="<?=$currentAdherent['idAdherent']; ?>"<?php endif; ?>>
                        </div>
                        <?php if ( isset($currentExemplaire['noExemplaire']) ): ?>
                        <div class="form-group col-md-12">
                            <label class="font-weight-bold" for="dateEmprunt">Date d'emprunt</label>
                            <input id="dateEmprunt" type="text" class="form-control" name="dateEmprunt" <?php  if(isset($queryParameter['dateEmprunt'])):?> value="<?=$queryParameter['dateEmprunt']; ?>"<?php endif; ?> placeholder="saisir une date"
                            value="<?=date('d/m/Y')?>">
                            <?php if(isset($error['dateEmprunt'])): ?>
                                <small class="form-text font-weight-bold text-danger"><?=$error['dateEmprunt'] ?></small>
                            <?php endif; ?>
                            <input type="hidden" class="form-control" name="try" <?php  if(isset($try)):?> value="<?=$try; ?>"<?php else: ?> value="0"<?php endif; ?>>
                            <input type="hidden" class="form-control" name="idAdherent" <?php  if(isset($currentAdherent['idAdherent'])):?> value="<?=$currentAdherent['idAdherent']; ?>"<?php endif; ?>>
                            <button type="submit" class="btn btn-dark mt-3" name="validate" value="1">Ajouter l'emprunt</button>
                        </div>
                        <?php endif; ?>
                    </div>
                </form>
            </div>
        </div>
        <?php elseif ( $nbEmprunt >= 5 ): ?>
        <div class="alert alert-danger">
            <span><strong>Information:</strong> le nombre d'emprunt maximum est de 5.</span>
        </div>
        <?php endif; ?>
        <?php elseif ( $isAdded ): ?>
        <div class="alert alert-success">
            <p><strong>Information:</strong> l'emprunt a bien été ajouté.</p>
            <form action="/EmpruntAdd" method="post">
                <input type="hidden" class="form-control" name="idAdherent" <?php  if(isset($currentAdherent['idAdherent'])):?> value="<?=$currentAdherent['idAdherent']; ?>"<?php endif; ?>>
                <button type="submit" class="btn btn-success">Retour</i></button>
            </form>
        </div>
        <?php endif; ?>
        <?php elseif (isset($currentAdherent)): ?>
        <div class="alert alert-danger">
            <p><strong>Information:</strong> plus aucun exemplaire de disponible.</p>
        </div>
        <?php endif; ?>
    </div>
    <?php if ( isset($currentAdherent['idAdherent']) ): ?>
    <?php if ($nbEmprunt == 0): ?>
    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="alert alert-dark">
            <span><strong>Information:</strong> aucun emprunt en cours pour cet adhérent.</span>
        </div>
    </div>
    <?php else: ?>
    <div class="col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
        <div class="alert alert-light text-center">
            <h4><strong>Emprunts de M./Mme <?=$currentAdherent['nomAdherent']?></strong></h4>
        </div>
        <table class="table table-responsive-lg">
            <thead class="thead-dark">
            <tr class="text-center" >
                <th scope="col">Titre</th>
                <th scope="col">Date d'emprunt</th>
                <th scope="col">Nombre jours</th>
                <th scope="col">Exemplaire</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($arrEmprunt as $row): ?>
                <tr class="text-center">
                    <th scope="row"><?=$row['titre']; ?></th>
                    <td><?=$row['dateEmprunt']; ?></td>
                    <td><?=$row['nbJour']; ?></td>
                    <td><?=$row['noExemplaire']; ?></td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div>
    <?php endif; ?>
    <?php endif; ?>
</div>
