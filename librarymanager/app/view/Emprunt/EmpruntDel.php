<?php include 'controller/Emprunt/EmpruntDelController.php'; ?>

<div class="row">
    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card my-2">
            <div class="card-body">
                <form action="/EmpruntDel" method="post">
                    <label for="idAdherent">Adhérent</label>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <select id="idAdherent" class="form-control" name="idAdherent" onchange="this.form.submit()">
                                <option value="0">Tous les adhérents</option>
                                <?php foreach ($arrAdherent as $row): ?>
                                <option value="<?=$row['idAdherent']?>"
                                    <?php if ( isset($currentAdherent['idAdherent']) ): ?>
                                    <?php if ( $currentAdherent['idAdherent'] == $row['idAdherent'] ): ?>
                                        selected
                                    <?php endif;  ?>
                                    <?php endif;  ?>
                                >
                                    <?=$row['nomAdherent']?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <form action="/EmpruntDel" method="post">
            <?php  if(isset($currentAdherent['idAdherent'])):?>
            <input type="hidden" class="form-control" name="idAdherent" value="<?=$currentAdherent['idAdherent']; ?>">
            <?php endif; ?>
            <button type="submit" class="btn btn-danger btn-block my-1" name="validate" value="1">Supprimer la sélection</button>
            <input type="checkbox" name="selectAll" id="selectAll" value="1" onchange="this.form.submit()"
                <?php if ( isset($_POST['selectAll']) ): ?> checked <?php endif; ?>>
            <label for="selectAll">Tout sélectionner</label>
            <table class="table table-responsive-lg">
                <thead class="thead-dark">
                <tr class="text-center" >
                    <th scope="col">Nom de l'adhérent</th>
                    <th scope="col">Titre</th>
                    <th scope="col">Date d'emprunt</th>
                    <th scope="col">Date de restitution</th>
                    <th scope="col">N° exemplaire</th>
                    <th scope="col">Jours d'emprunt</th>
                    <th scope="col">Supprimer</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($arrEmprunt as $row): ?>
                    <tr class="text-center">
                        <th scope="row"><?=$row['nomAdherent']; ?></th>
                        <td><?=$row['titre']; ?></td>
                        <td><?=$row['dateEmprunt']; ?></td>
                        <td><?=$row['dateRendu']; ?></td>
                        <td><?=$row['noExemplaire']; ?></td>
                        <td><?=$row['nbJour']; ?></td>
                        <td>
                            <label hidden for="emprunt"></label>
                            <input type="checkbox" name="arrEmpruntToDel[]" id="emprunt"
                                   value="<?php  if(isset($row['idAdherent'])):?><?=$row['idAdherent'];?><?php endif; ?>
                                    ,<?php  if(isset($row['dateEmprunt'])):?><?=$row['dateEmprunt'];?><?php endif; ?>
                                    ,<?php  if(isset($row['noExemplaire'])):?><?=$row['noExemplaire'];?><?php endif; ?>
                                   "
                                <?php if ( isset($_POST['selectAll']) ): ?> checked <?php endif; ?>
                            >
                        </td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
        </form>
    </div>
</div>