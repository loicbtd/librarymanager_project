<?php include 'controller/Emprunt/EmpruntReturnController.php'; ?>

<div class="row">
    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card my-2">
            <div class="card-body">
                <form action="/EmpruntReturn" method="post">
                    <label for="idAdherent">Adhérent</label>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <select id="idAdherent" class="form-control" name="idAdherentCur" onchange="this.form.submit()">
                                <option value="0">Tous les adhérents</option>
                                <?php foreach ($arrAdherent as $row): ?>
                                    <option value="<?=$row['idAdherent']?>"
                                        <?php if ( isset($currentAdherent['idAdherent']) ): ?>
                                            <?php if ( $currentAdherent['idAdherent'] == $row['idAdherent'] ): ?>
                                                selected
                                            <?php endif;  ?>
                                        <?php endif;  ?>
                                        >
                                        <?=$row['nomAdherent']?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <?php if(isset($error['dateRendu'])):?>
        <div class="alert alert-danger">
            <span><srong class="font-weight-bold">Information: </srong><?=$error['dateRendu']?></span>
        </div>
        <?php endif;?>
        <table class="table table-responsive-md text-center text-nowrap">
            <thead class="thead-dark">
            <tr class="text-center" >
                <th scope="col">Nom de l'adhérent</th>
                <th scope="col">Titre</th>
                <th scope="col">Date d'emprunt</th>
                <th scope="col">N° exemplaire</th>
                <th scope="col">Jours d'emprunt</th>
                <th scope="col"><span class="px-3 text-nowrap">Date rendu</span></th>
                <th scope="col">Rendre</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($arrEmprunt as $row): ?>
                <form action="/EmpruntReturn" method="post">
                <tr class="text-center">
                    <th scope="row"><?=$row['nomAdherent']; ?></th>
                    <td><?=$row['titre']; ?></td>
                    <td><?=$row['dateEmprunt']; ?></td>
                    <td><?=$row['noExemplaire']; ?></td>
                    <td><?=$row['nbJour']; ?></td>
                    <td>
                        <label hidden for="dateRendu"></label>
                        <input id="dateRendu" type="text" class="form-control" name="dateRendu"  placeholder="JJ/MM/AAAA" value=<?=date('d/m/Y')?>>
                    </td>
                    <td>
                        <?php  if(isset($currentAdherent['idAdherent'])):?>
                            <input type="hidden" class="form-control" name="idAdherentCur" value="<?=$currentAdherent['idAdherent']; ?>">
                        <?php endif; ?>
                        <input type="hidden" class="form-control" name="idAdherent" <?php  if(isset($row['idAdherent'])):?> value="<?=$row['idAdherent']; ?>"<?php endif; ?>>
                        <input type="hidden" class="form-control" name="noExemplaire" <?php  if(isset($row['noExemplaire'])):?> value="<?=$row['noExemplaire']; ?>"<?php endif; ?>>
                        <input type="hidden" class="form-control" name="dateEmprunt" <?php  if(isset($row['dateEmprunt'])):?> value="<?=$row['dateEmprunt']; ?>"<?php endif; ?>>
                        <button type="submit" class="btn btn-success mx-1" name="validate">Rendre</button>
                    </td>
                </tr>
                </form>
            <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>