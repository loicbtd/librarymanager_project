<?php include 'controller/Emprunt/EmpruntBilanController.php'; ?>

<div class="row">
    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <form action="/EmpruntBilan" method="post">
            <table class="table table-responsive-lg">
                <thead class="thead-dark">
                <tr class="text-center" >
                    <th scope="col">Nom de l'adhérent</th>
                    <th scope="col">Titre</th>
                    <th scope="col">Date d'emprunt</th>
                    <th scope="col">N° exemplaire</th>
                    <th scope="col">Retard (jours)</th>
                    <th scope="col">Pénalités</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($arrBilan as $row): ?>
                    <tr class="text-center
                    <?php if ( $row['flagPenalite'] == "1" ): ?>
                    table-danger
                    <?php elseif ( $row['flagRetard'] == "1" ): ?>
                    table-warning
                    <?php endif; ?>
                    ">
                        <th scope="row"><?=$row['nomAdherent']; ?></th>
                        <td><?=$row['titre']; ?></td>
                        <td><?=$row['dateEmprunt']; ?></td>
                        <td><?=$row['noExemplaire']; ?></td>
                        <td><?=$row['retard']; ?></td>
                        <td><?=$row['dette']." €"; ?></td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
            <button type="submit" class="btn btn-dark btn-block" name="sendMail" value="1">Envoyer des emails de rappel</button>
        </form>
    </div>
</div>

