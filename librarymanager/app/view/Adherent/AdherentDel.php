<?php include 'controller/Adherent/AdherentDelController.php'; ?>

<div class="row">
    <?php if( isset($error['nbEmprunt']) ): ?>
    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="alert alert-danger" role="alert">
            <p><strong>Erreur:</strong> impossible de supprimer cet adhérent car encore <strong><?=$error['nbEmprunt']?> emprunt(s)</strong> lui font référence.</p>
            <a href="/AdherentShow" class="btn btn-danger m-1" tabindex="-1" role="button" aria-disabled="true">Retour</a>
        </div>
    </div>
    <?php elseif($isDeleted): ?>
    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="alert alert-success" role="alert">
            <p><strong>Information:</strong> l'adhérent a bien été supprimé.</p>
            <a href="/AdherentShow" class="btn btn-success m-1" tabindex="-1" role="button" aria-disabled="true">Retour</a>
        </div>
    </div>
    <?php else: ?>
    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card">
            <div class="card-body">
                <h2 class="card-title">Information</h2>
                <p class="text-danger font-weight-bold">Cette action est irreversible.</p>
                <p class="font-weight-bold">Voulez-vous vraiment supprimer cet adhérent ? </p>
                <form id="form-button" action="/AdherentDel" method="post">
                    <input type="hidden" class="form-control" name="idAdherent" <?php  if(isset($queryParameter['idAdherent'])):?> value="<?=$queryParameter['idAdherent']; ?>"<?php endif; ?>>
                    <input type="hidden" class="form-control" name="validate" value="y">
                    <button type="submit" class="btn btn btn-danger m-1">Supprimer</button>
                </form>
                <a href="/AdherentShow" class="btn btn-dark m-1" tabindex="-1" role="button" aria-disabled="true">Retour</a>
            </div>
        </div>
    </div>
    <?php endif; ?>
</div>