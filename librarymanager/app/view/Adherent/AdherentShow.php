<?php include 'controller/Adherent/AdherentShowController.php'; ?>

<div class="row">
    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <a href="/AdherentAdd" class="btn btn-block btn-success my-1" tabindex="-1" role="button" aria-disabled="true"><i class="fas fa-plus"></i></a>
    </div>
    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <table class="table table-responsive-lg">
            <thead class="thead-dark">
            <tr class="text-center" >
                <th scope="col">Nom</th>
                <th scope="col">Adresse</th>
                <th scope="col">Date paiement</th>
                <th scope="col">Nombre d'emprunts</th>
                <th scope="col">Informations</th>
                <th scope="col">Opérations</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($queryAnswer as $row): ?>
                <tr class="text-center">
                    <th scope="row"><?=$row['nomAdherent']; ?></th>
                    <td><?=$row['adresse']; ?></td>
                    <td><?=$row['datePaiement']; ?></td>
                    <td><?=$row['nbEmpruntEnCours'];?></td>
                    <td>
                        <!-- ici -->
                        <?php if((int)$row['flagRetardProche']==1):?>
                            <span class="text-danger">Paiement à renouveler</span>
                            <br>
                        <?php elseif((int)$row['flagRetard']==1):?>
                            <span class="text-danger">Paiement en retard depuis le <?=$row['dateButoir']?></span>
                            <br>
                        <?php endif;?>
                    </td>
                    <td scope="col">
                        <div class="d-inline-flex">
                            <form class="w-50" action="/AdherentEdit" method="post">
                                <input type="hidden" class="form-control" name="idAdherent" <?php  if(isset($row['idAdherent'])):?> value="<?=$row['idAdherent']; ?>"<?php endif; ?>>
                                <input type="hidden" class="form-control" name="nomAdherent"<?php  if(isset($row['nomAdherent'])):?> value="<?=$row['nomAdherent']; ?>"<?php endif; ?>>
                                <input type="hidden" class="form-control" name="adresse" <?php  if(isset($row['adresse'])):?> value="<?=$row['adresse']; ?>"<?php endif; ?>>
                                <input type="hidden" class="form-control" name="datePaiement" <?php  if(isset($row['datePaiement'])):?> value="<?=$row['datePaiement']; ?>"<?php endif; ?>>
                                <input type="hidden" class="form-control" name="email" <?php  if(isset($row['email'])):?> value="<?=$row['email']; ?>"<?php endif; ?>>
                                <button type="submit" class="btn btn-sm btn-dark mx-1"><i class="fas fa-edit"></i></button>
                            </form>
                            <form class="w-50" action="/AdherentDel" method="post">
                                <input type="hidden" class="form-control" name="idAdherent" <?php  if(isset($row['idAdherent'])):?> value="<?=$row['idAdherent']; ?>"<?php endif; ?>>
                                <button type="submit" class="btn btn-sm btn-dark mx-1"><i class="fas fa-trash"></i></button>
                            </form>
                        </div>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>