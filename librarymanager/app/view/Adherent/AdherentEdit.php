<?php include 'controller/Adherent/AdherentEditController.php'; ?>

<div class="row">
    <?php if(!$isModified): ?>
    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <form action="/AdherentEdit" method="post">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label class="font-weight-bold" for="nomAdherent" >Nom</label>
                    <input type="text" class="form-control" name="nomAdherent" <?php  if(isset($queryParameter['nomAdherent'])):?> value="<?=$queryParameter['nomAdherent']; ?>"<?php endif; ?> placeholder="saisir un nom">
                    <?php if(isset($error['nomAdherent'])): ?>
                        <small class="form-text font-weight-bold text-danger"><?=$error['nomAdherent'] ?></small>
                    <?php endif; ?>
                </div>
                <div class="form-group col-md-6">
                    <label class="font-weight-bold" for="adresse">Adresse</label>
                    <input type="text" class="form-control" name="adresse" <?php  if(isset($queryParameter['adresse'])):?> value="<?=$queryParameter['adresse']; ?>"<?php endif; ?> placeholder="saisir une adresse">
                    <?php if(isset($error['adresse'])): ?>
                        <small class="form-text font-weight-bold text-danger"><?=$error['adresse'] ?></small>
                    <?php endif; ?>
                </div>
                <div class="form-group col-md-6">
                    <label class="font-weight-bold" for="email">Email</label>
                    <input type="text" class="form-control" name="email" <?php  if(isset($queryParameter['email'])):?> value="<?=$queryParameter['email']; ?>"<?php endif; ?> placeholder="saisir l'email">
                    <?php if(isset($error['email'])): ?>
                        <small class="form-text font-weight-bold text-danger"><?=$error['email'] ?></small>
                    <?php endif; ?>
                </div>
                <div class="form-group col-md-6">
                    <label class="font-weight-bold" for="datePaiement">Date de paiement</label>
                    <input id="datePaiement" type="text" class="form-control" name="datePaiement" <?php  if(isset($queryParameter['datePaiement'])):?> value="<?=$queryParameter['datePaiement']; ?>"<?php endif; ?> placeholder="JJ/MM/AAAA"
                           value="<?=date('d/m/Y')?>">
                    <?php if(isset($error['datePaiement'])): ?>
                        <small class="form-text font-weight-bold text-danger"><?=$error['datePaiement'] ?></small>
                    <?php endif; ?>
                </div>
            </div>
            <input type="hidden" class="form-control" name="idAdherent" <?php  if(isset($queryParameter['idAdherent'])):?> value="<?=$queryParameter['idAdherent']; ?>"<?php endif; ?>>
            <input type="hidden" class="form-control" name="validate" value="y">
            <button type="submit" class="btn btn-dark m-1">Valider</button>
            <a href="/AdherentShow" class="btn btn-light m-1" tabindex="-1" role="button" aria-disabled="true">Retour</a>
        </form>
    </div>
    <?php else: ?>
    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="alert alert-success" role="alert">
            <p><strong>Information:</strong> l'adhérent a bien été modifié.</p>
            <a href="/AdherentShow" class="btn btn-success m-1" tabindex="-1" role="button" aria-disabled="true">Retour</a>
        </div>
    </div>
    <?php endif; ?>
</div>