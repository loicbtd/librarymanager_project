<?php
include 'resource/conf/db.php';

/**
 * @param $idQuery
 * @param $option
 * @return array
 */
function dashboard($idQuery) {
    switch ($idQuery) {
        case "emprunt_ok":
            $cmd = "
                SELECT COUNT(*) as data
                FROM EMPRUNT
                WHERE dateRendu IS NULL 
                  AND CURRENT_DATE() <= DATE_ADD(dateEmprunt, INTERVAL 90 DAY);
                ";
            break;

        case "emprunt_retard":
            $cmd = "
                SELECT COUNT(*) as data
                FROM EMPRUNT
                WHERE dateRendu IS NULL 
                  AND CURRENT_DATE() > DATE_ADD(dateEmprunt, INTERVAL 90 DAY);
                ";
            break;

        case "adherent_ok":
            $cmd = "
                SELECT COUNT(*) as data
                FROM ADHERENT
                WHERE CURRENT_DATE() < DATE_ADD(datePaiement, INTERVAL 1 YEAR);
                ";
            break;

        case "adherent_retard":
            $cmd = "
                SELECT COUNT(*) as data
                FROM ADHERENT
                WHERE CURRENT_DATE() >= DATE_ADD(datePaiement, INTERVAL 1 YEAR);
                ";
            break;

        case "auteur":
            $cmd = "
                SELECT AUTEUR.idAuteur, AUTEUR.nomAuteur, COUNT(OEUVRE.noOeuvre) AS nbOeuvre
                FROM AUTEUR
                INNER JOIN OEUVRE ON OEUVRE.idAuteur = AUTEUR.idAuteur
                GROUP BY AUTEUR.idAuteur
                ORDER BY AUTEUR.nomAuteur;
                ";
            break;

        case "oeuvre":
            $cmd = "
                SELECT AUTEUR.nomAuteur
                , OEUVRE.titre
                , COUNT(E1.noExemplaire) AS nbTotal
                , COUNT(E2.noExemplaire) AS nbDispo
                FROM OEUVRE
                INNER JOIN AUTEUR ON OEUVRE.idAuteur = AUTEUR.idAuteur
                LEFT JOIN EXEMPLAIRE E1 ON OEUVRE.noOeuvre = E1.noOeuvre
                LEFT JOIN EXEMPLAIRE E2 ON E1.noExemplaire = E2.noExemplaire
                  AND E2.noExemplaire NOT IN (
                    SELECT EMPRUNT.noExemplaire
                    FROM EMPRUNT
                    WHERE EMPRUNT.dateRendu IS NULL)
                GROUP BY OEUVRE.titre
                ORDER BY AUTEUR.nomAuteur;
                ";
            break;
    }
    // connexion à la base de données :
    $dsn = 'mysql:dbname='.database.';host='.hostname.';charset=utf8';
    // création d'une instance d'un objet PDO de nom $bdd
    $db = new PDO($dsn, username, password);
    // pour afficher les erreurs
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // pour récupérer le résultat des requêtes SELECT sous forme de tableaux associatifs
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_ASSOC);
    // execution de la commande :
    $answer = $db->query($cmd);
    // recupération des données sous la forme d'un tableau associatif :
    return $answer->fetchAll();
}
?>

<script>
    // chartEmprunt
    window.onload = function () {
        var chartEmprunt = new CanvasJS.Chart("chartEmprunt", {
            animationEnabled: true,
            theme: "light2",
            data: [{
                type: "doughnut",
                startAngle: 60,
                innerRadius: 90,
                indexLabelFontSize: 15,
                indexLabel: "{label} : {y}",
                toolTipContent: "<b>{label}:</b>{y}",
                dataPoints: [
                    { y: <?=dashboard("emprunt_ok")[0]["data"]?>, label: "en règle" },
                    { y: <?=dashboard("emprunt_retard")[0]["data"]?> , label: "retard" },
                ]
            }]
        });
        chartEmprunt.render();

        // chartAdherent
        var chartAdherent = new CanvasJS.Chart("chartAdherent", {
            animationEnabled: true,
            theme: "light2",
            data: [{
                type: "doughnut",
                startAngle: 60,
                innerRadius: 90,
                indexLabelFontSize: 15,
                indexLabel: "{label} : {y}",
                toolTipContent: "<b>{label}:</b>{y}",
                dataPoints: [
                    { y: <?=dashboard("emprunt_ok")[0]["data"]?>, label: "en règle" },
                    { y: <?=dashboard("emprunt_retard")[0]["data"]?>, label: "retard" },
                ]
            }]
        });
        chartAdherent.render();

        // chartAuteur
        var chartAuteur = new CanvasJS.Chart("chartAuteur",
            {
                animationEnabled: true,
                theme: "light2",
                title:{
                    text: "Nombre d'oeuvres par auteur"
                },
                axisX:{
                    interval: 1
                },
                data: [
                    {
                        dataPoints: [
                            <?php $data = dashboard("auteur"); ?>
                            <?php for($i = 0; $i < sizeof($data)-1; $i++): ?>
                            { x: <?=$i+10?>, y: <?=$data[$i]["nbOeuvre"]?>, label: "<?=$data[$i]["nomAuteur"]?>"  },
                            <?php endfor; ?>
                        ]
                    }
                ]
            });
        chartAuteur.render();

        // chartOeuvre
        var chartOeuvre = new CanvasJS.Chart("chartOeuvre",
            {
                animationEnabled: true,
                theme: "light2",
                title:{
                    text: "Exemplaires disponibles et empruntés"
                },
                axisX:{
                    interval: 1
                },
                data: [
                    {
                        type: "stackedColumn",
                        dataPoints: [
                            <?php $oeuvre = dashboard("oeuvre"); ?>
                            <?php for($i = 0; $i < sizeof($oeuvre)-1; $i++): ?>
                            { x: <?=$i?>, y: <?=$oeuvre[$i]["nbDispo"]?> , label: "disponibles" },
                            <?php endfor; ?>
                        ]
                    }
                    ,{
                        type: "stackedColumn",
                        dataPoints: [
                            <?php $oeuvre = dashboard("oeuvre"); ?>
                            <?php for($i = 0; $i < sizeof($oeuvre)-1; $i++): ?>
                            { x: <?=$i?>, y: <?=$oeuvre[$i]["nbTotal"]-$oeuvre[$i]["nbDispo"]?>, label: "empruntés" },
                            <?php endfor; ?>
                        ]
                    }
                    ,{
                        type: "stackedColumn", // change here (a)
                        dataPoints: [

                            <?php $oeuvre = dashboard("oeuvre"); ?>
                            <?php for($i = 0; $i < sizeof($oeuvre)-1; $i++): ?>
                            { x: <?=$i?>, label: "<?=$oeuvre[$i]["titre"]?> - <?=$oeuvre[$i]["nomAuteur"]?>"  },
                            <?php endfor; ?>
                        ]
                    }
                ]
            });
        chartOeuvre.render();
    };
</script>

<div class="row">
    <div class="col-12">
        <div class="card my-1">
            <img src="" class="card-img-top" alt="">
            <div class="card-body">
                <h5 class="card-title font-weight-bold">Oeuvres</h5>
                <div id="chartOeuvre" style="height: 250px; width: 100%;"></div>
                <a href="/OeuvreShow" class="btn btn-primary btn-dark"><i class="fas fa-external-link-alt"></i></a>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="card my-1">
            <img src="" class="card-img-top" alt="">
            <div class="card-body">
                <h5 class="card-title font-weight-bold">Auteurs</h5>
                <div id="chartAuteur" style="height: 250px; width: 100%;"></div>
                <a href="/AuteurShow" class="btn btn-primary btn-dark"><i class="fas fa-external-link-alt"></i></a>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-6">
        <div class="card my-1">
            <img src="" class="card-img-top" alt="">
            <div class="card-body my-1">
                <h5 class="card-title font-weight-bold">Emprunts</h5>
                <div id="chartEmprunt" style="height: 250px; width: 100%;"></div>
                <a href="/EmpruntShow" class="btn btn-primary btn-dark"><i class="fas fa-external-link-alt"></i></a>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-6">
        <div class="card my-1">
            <img src="" class="card-img-top" alt="">
            <div class="card-body">
                <h5 class="card-title font-weight-bold">Adhérents</h5>
                <div id="chartAdherent" style="height: 250px; width: 100%;"></div>
                <a href="/AdherentShow" class="btn btn-primary btn-dark"><i class="fas fa-external-link-alt"></i></a>
            </div>
        </div>
    </div>
</div>

