<?php include 'controller/Exemplaire/ExemplaireDelController.php'; ?>

<div class="row">
    <?php if( $nbEmprunt != 0 ): ?>
        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="alert alert-danger" role="alert">
                <?php if( $nbEmprunt > 1 ): ?>
                    <p><strong>Attention:</strong> il est dangereux de supprimer cet exemplaire car <strong><?=$nbEmprunt?></strong> emprunts lui font référence.<br>La suppression de cet exemplaire entrainera la suppresion des emprunts associés.</p>
                <?php else: ?>
                    <p><strong>Attention:</strong> il est dangereux de supprimer cet exemplaire car un emprunt lui fait référence.<br>La suppression de cet exemplaire entrainera la suppression de l'emprunt associé.</p>
                <?php endif; ?>
                <form class="d-inline-flex" action="/ExemplaireDel" method="post">
                    <input type="hidden" class="form-control" name="noExemplaire" <?php  if(isset($queryParameter['noExemplaire'])):?> value="<?=$queryParameter['noExemplaire']; ?>"<?php endif; ?>>
                    <input type="hidden" class="form-control" name="noOeuvre" <?php  if(isset($queryParameter['noOeuvre'])):?> value="<?=$queryParameter['noOeuvre']; ?>"<?php endif; ?>>
                    <input type="hidden" class="form-control" name="origin" <?php  if(isset($origin)):?> value="<?=$origin; ?>"<?php endif; ?>>
                    <input type="hidden" class="form-control" name="confirmation" value="1">
                    <button type="submit" class="btn btn-danger">Supprimer tout de même</i></button>
                </form>
                <form class="d-inline-flex" action="/<?=$origin?>" method="post">
                    <input type="hidden" class="form-control" name="noExemplaire" <?php  if(isset($queryParameter['noExemplaire'])):?> value="<?=$queryParameter['noExemplaire']; ?>"<?php endif; ?>>
                    <input type="hidden" class="form-control" name="noOeuvre" <?php  if(isset($queryParameter['noOeuvre'])):?> value="<?=$queryParameter['noOeuvre']; ?>"<?php endif; ?>>
                    <button type="submit" class="btn btn-light">Retour</i></button>
                </form>
            </div>
        </div>
    <?php elseif($isDeleted): ?>
        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="alert alert-success" role="alert">
                <p><strong>Information:</strong> l'exemplaire a bien été supprimé.</p>
                <form action="/<?=$origin?>" class="d-inline-flex" method="post">
                    <input type="hidden" class="form-control" name="noExemplaire" <?php  if(isset($queryParameter['noExemplaire'])):?> value="<?=$queryParameter['noExemplaire']; ?>"<?php endif; ?>>
                    <input type="hidden" class="form-control" name="noOeuvre" <?php  if(isset($queryParameter['noOeuvre'])):?> value="<?=$queryParameter['noOeuvre']; ?>"<?php endif; ?>>
                    <button type="submit" class="btn btn-success">Retour</i></button>
                </form>
            </div>
        </div>
    <?php else: ?>
        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="alert alert-danger" role="alert">
                <p><strong>Information:</strong> Cette action est irreversible. Voulez-vous vraiment supprimer cette oeuvre ?</p>
                <form class="d-inline-flex" action="/ExemplaireDel" method="post">
                    <input type="hidden" class="form-control" name="noExemplaire" <?php  if(isset($queryParameter['noExemplaire'])):?> value="<?=$queryParameter['noExemplaire']; ?>"<?php endif; ?>>
                    <input type="hidden" class="form-control" name="noOeuvre" <?php  if(isset($queryParameter['noOeuvre'])):?> value="<?=$queryParameter['noOeuvre']; ?>"<?php endif; ?>>
                    <input type="hidden" class="form-control" name="origin" <?php  if(isset($origin)):?> value="<?=$origin; ?>"<?php endif; ?>>
                    <input type="hidden" class="form-control" name="confirmation" value="1">
                    <button type="submit" class="btn btn-danger">Supprimer</i></button>
                </form>
                <form action="/<?=$origin?>" class="d-inline-flex" method="post">
                    <input type="hidden" class="form-control" name="noExemplaire" <?php  if(isset($queryParameter['noOeuvre'])):?> value="<?=$queryParameter['noOeuvre']; ?>"<?php endif; ?>>
                    <input type="hidden" class="form-control" name="noOeuvre" <?php  if(isset($queryParameter['noOeuvre'])):?> value="<?=$queryParameter['noOeuvre']; ?>"<?php endif; ?>>
                    <button type="submit" class="btn btn-light">Retour</i></button>
                </form>
            </div>
        </div>
    <?php endif; ?>
</div>