<?php include 'controller/Exemplaire/ExemplaireAddController.php'; ?>

<div class="row">
    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mb-1">
        <div class="infoButton">
            <div class="d-inline-block">
                <span class="badge badge-dark">Titre</span>
                <span class="badge badge-light"><?=$currentOeuvre['titre']?></span>
            </div>
            <div class="d-inline-block">
                <span class="badge badge-dark">Auteur</span>
                <span class="badge badge-light"><?=$currentOeuvre['nomAuteur']." ".$currentOeuvre['prenomAuteur']?></span>
            </div>
            <div class="d-inline-block">
                <span class="badge badge-dark">Date de parution</span>
                <span class="badge badge-light"><?=$currentOeuvre['dateParution']?></span>
            </div>
        </div>
    </div>
    <?php if(!$isAdded): ?>
    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <form method="post">
            <div class="form-row">
                <div class="form-group col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <fieldset class="form-group form-check-inline">
                        <legend><h6 class="font-weight-bold">État</h6></legend>
                        <input hidden class="form-check-input" type="radio" name="etat" id="etat-empty" value="0"
                            <?php if( !isset($queryParameter['etat']) ): ?>
                                checked
                            <?php endif; ?>
                        >
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="etat" id="etat-neuf" value="neuf"
                                <?php if( isset($queryParameter['etat']) && $queryParameter['etat']=="neuf"): ?>
                                    checked
                                <?php endif; ?>
                            >
                            <label class="form-check-label" for="etat-neuf">neuf</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="etat" id="etat-bon" value="bon"
                                <?php if( isset($queryParameter['etat']) && $queryParameter['etat']=="bon"): ?>
                                    checked
                                <?php endif; ?>
                            >
                            <label class="form-check-label" for="etat-bon">bon</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="etat" id="etat-moyen" value="moyen"
                                <?php if( isset($queryParameter['etat']) && $queryParameter['etat']=="moyen"): ?>
                                    checked
                                <?php endif; ?>
                            >
                            <label class="form-check-label" for="etat-moyen">moyen</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="etat" id="etat-mauvais" value="mauvais"
                                <?php if( isset($queryParameter['etat']) && $queryParameter['etat']=="mauvais"): ?>
                                    checked
                                <?php endif; ?>
                            >
                            <label class="form-check-label" for="etat-mauvais">mauvais</label>
                        </div>
                    </fieldset>
                    <?php  if(isset($error['etat'])): ?>
                        <small class="form-text font-weight-bold text-danger"><?=$error['etat'] ?></small>
                    <?php  endif; ?>
                </div>
                <div class="form-group col-12 col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                    <div class="form-group">
                        <label class="font-weight-bold" for="dateAchat">Date d'achat</label>
                        <input id="dateAchat" type="text" class="form-control" name="dateAchat" <?php  if(isset($queryParameter['dateAchat'])):?> value="<?=$queryParameter['dateAchat']; ?>"<?php endif;?>value="<?=date('d/m/Y')?>" placeholder="JJ/MM/AAAA">
                        <?php if(isset($error['dateAchat'])): ?>
                            <small class="form-text font-weight-bold text-danger"><?=$error['dateAchat'] ?></small>
                        <?php endif; ?>
                    </div>
                    <div class="form-group">
                        <label class="font-weight-bold" for="prix">Prix</label>
                        <input id="prix" type="text" class="form-control" name="prix" <?php  if(isset($queryParameter['prix'])):?> value="<?=$queryParameter['prix']; ?>"<?php endif; ?> placeholder="saisir un prix">
                        <?php if(isset($error['prix'])): ?>
                            <small class="form-text font-weight-bold text-danger"><?=$error['prix'] ?></small>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <input type="hidden" class="form-control" name="noOeuvre" <?php  if(isset($queryParameter['noOeuvre'])):?> value="<?=$queryParameter['noOeuvre']; ?>"<?php endif; ?>>
            <button class="btn btn-dark" formaction="/ExemplaireAdd">Valider</button>
            <button class="btn btn-light" formaction="/ExemplaireShow">Retour</button>
        </form>
    </div>
    <?php else: ?>
        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="alert alert-success" role="alert">
                <p><strong>Information:</strong> l'exemplaire a bien été ajouté.</p>
                <form method="post">
                    <input type="hidden" class="form-control" name="noOeuvre" <?php  if(isset($queryParameter['noOeuvre'])):?> value="<?=$queryParameter['noOeuvre']; ?>"<?php endif; ?>>
                    <button type="submit" class="btn btn-sm btn-success mr-1" formaction="/ExemplaireAdd">Nouveau</button>
                    <button type="submit" class="btn btn-sm btn-light ml-1" formaction="/ExemplaireShow">Retour</button>
                </form>
            </div>
        </div>
    <?php endif; ?>
</div>