<?php include 'controller/Exemplaire/ExemplaireShowController.php' ?>

<div class="row">
    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 mb-1">
        <div class="row">
            <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="infoButton">
                    <div class="d-inline-block">
                        <span class="badge badge-dark">Titre</span>
                        <span class="badge badge-light"><?=$currentOeuvre['titre']?></span>
                    </div>
                   <div class="d-inline-block">
                       <span class="badge badge-dark">Auteur</span>
                       <span class="badge badge-light"><?=$currentOeuvre['nomAuteur']." ".$currentOeuvre['prenomAuteur']?></span>
                   </div>
                    <div class="d-inline-block">
                        <span class="badge badge-dark">Date de parution</span>
                        <span class="badge badge-light"><?=$currentOeuvre['dateParution']?></span>
                    </div>
                    <div class="d-inline-block">
                        <span class="badge badge-dark">Total</span>
                        <span class="badge badge-light"><?=$currentOeuvre['nbTotal']?></span>
                    </div>
                    <div class="d-inline-block">
                        <span class="badge badge-success">Disponible</span>
                        <span class="badge badge-light"><?=$currentOeuvre['nbDispo']?></span>
                    </div>
                </div>
            </div>
            <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="row">
                    <div class="col-8 col-xs-8 col-sm-8 col-md-8 col-lg-8 col-xl-8">
                        <form action="/ExemplaireAdd" method="post">
                            <input type="hidden" class="form-control" name="noOeuvre" <?php  if(isset($queryParameter['noOeuvre'])):?> value="<?=$queryParameter['noOeuvre']; ?>"<?php endif; ?>>
                            <button type="submit" class="btn btn-success btn-block"><i class="fas fa-plus"></i></button>
                        </form>
                    </div>
                    <div class="col-4 col-xs-4 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                        <a href="/OeuvreShow" class="btn btn-dark btn-block" tabindex="-1" role="button" aria-disabled="true">Retour</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if($nbExemplaire != 0): ?>
    <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <table class="table table-responsive-sm">
            <thead class="thead-dark">
            <tr class="text-center" >
                <th scope="col">N°</th>
                <th scope="col">Disponibilité</th>
                <th scope="col">État</th>
                <th scope="col">Date d'achat</th>
                <th scope="col">Prix</th>
                <th scope="col">Opérations</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($queryAnswer as $row): ?>
                <tr class="text-center">
                    <th scope="row"><?=$row['noExemplaire']; ?></th>
                    <td>
                        <?php if($row['flagPresent']=="1"): ?>
                        <span class="text-success">disponible</span>
                        <?php else: ?>
                        <span class="text-danger">emprunté</span>
                        <?php endif; ?>
                    </td>
                    <td><?=$row['etat']; ?></td>
                    <td><?=$row['dateAchat']; ?></td>
                    <td><?=number_format((float)$row['prix'], 2, ",", " ")." €"?></td>
                    <td scope="col">
                        <div class="d-inline-flex">
                            <form class="w-50" action="/ExemplaireEdit" method="post">
                                <input type="hidden" class="form-control" name="noOeuvre" <?php  if(isset($queryParameter['noOeuvre'])):?> value="<?=$queryParameter['noOeuvre']; ?>"<?php endif; ?>>
                                <input type="hidden" class="form-control" name="noExemplaire" <?php  if(isset($row['noExemplaire'])):?> value="<?=$row['noExemplaire']; ?>"<?php endif; ?>>
                                <input type="hidden" class="form-control" name="etat" <?php  if(isset($row['etat'])):?> value="<?=$row['etat']; ?>"<?php endif; ?>>
                                <input type="hidden" class="form-control" name="dateAchat" <?php  if(isset($row['dateAchat'])):?> value="<?=$row['dateAchat']; ?>"<?php endif; ?>>
                                <input type="hidden" class="form-control" name="prix" <?php  if(isset($row['prix'])):?> value="<?=$row['prix']; ?>"<?php endif; ?>>
                                <button type="submit" class="btn btn-sm btn-dark mx-1"><i class="fas fa-edit"></i></button>
                            </form>
                            <form class="w-50" action="/ExemplaireDel" method="post">
                                <input type="hidden" class="form-control" name="noExemplaire" <?php  if(isset($row['noExemplaire'])):?> value="<?=$row['noExemplaire']; ?>"<?php endif; ?>>
                                <input type="hidden" class="form-control" name="noOeuvre" <?php  if(isset($queryParameter['noOeuvre'])):?> value="<?=$queryParameter['noOeuvre']; ?>"<?php endif; ?>>
                                <input type="hidden" class="form-control" name="origin" value="ExemplaireShow">
                                <button type="submit" class="btn btn-sm btn-dark mx-1"><i class="fas fa-trash"></i></button>
                            </form>
                        </div>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
    </div>
    <?php else: ?>
        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <div class="alert alert-dark" role="alert">
                <span>Aucun exemplaire de cette oeuvre n'existe dans la bibliothèque.</span>
            </div>
        </div>
    <?php endif; ?>
</div>