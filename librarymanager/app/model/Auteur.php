<?php

include 'resource/conf/db.php';

/**
 * @param $idQuery
 * @param $option
 * @return array
 */
function select($idQuery,$option) {
    switch ($idQuery) {
        case 1:
            $cmd = "
                SELECT AUTEUR.idAuteur, AUTEUR.nomAuteur, AUTEUR.prenomAuteur, COUNT(OEUVRE.noOeuvre) AS nbOeuvre
                FROM OEUVRE
                RIGHT JOIN AUTEUR ON OEUVRE.idAuteur = AUTEUR.idAuteur
                GROUP BY AUTEUR.nomAuteur
                ORDER BY AUTEUR.nomAuteur, AUTEUR.prenomAuteur;
                ";
            break;

        case 2:
            $cmd = "
                SELECT COUNT(OEUVRE.noOeuvre) AS nbOeuvre
                FROM OEUVRE
                RIGHT JOIN AUTEUR ON OEUVRE.idAuteur = AUTEUR.idAuteur AND AUTEUR.idAuteur=".$option['idAuteur'].";
                ";
            break;
    }
    // connexion à la base de données :
    $dsn = 'mysql:dbname='.database.';host='.hostname.';charset=utf8';
    // création d'une instance d'un objet PDO de nom $bdd
    $db = new PDO($dsn, username, password);
    // pour afficher les erreurs
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // pour récupérer le résultat des requêtes SELECT sous forme de tableaux associatifs
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_ASSOC);
    // execution de la commande :
    $answer = $db->query($cmd);
    // recupération des données sous la forme d'un tableau associatif :
    return $answer->fetchAll();
}

/**
 * @param $idQuery
 * @param $option
 * @return int
 */
function insert($idQuery,$option) {
    switch ($idQuery) {
        case 1:
            $cmd = "
                INSERT INTO AUTEUR VALUES (NULL,'".$option['nomAuteur']."', '".$option['prenomAuteur']."');
                ";
            break;
    }
    // connexion à la base de données :
    $dsn = 'mysql:dbname='.database.';host='.hostname.';charset=utf8';
    // création d'une instance d'un objet PDO de nom $bdd
    $db = new PDO($dsn, username, password);
    // pour afficher les erreurs
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // pour récupérer le résultat des requêtes SELECT sous forme de tableaux associatifs
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_ASSOC);
    // execution de la commande :
    return $db->exec($cmd);
}

/**
 * @param $idQuery
 * @param $option
 * @return int
 */
function update($idQuery,$option) {
    switch ($idQuery) {
        case 1:
            $cmd = "
                UPDATE AUTEUR SET nomAuteur='".$option['nomAuteur']."', prenomAuteur='".$option['prenomAuteur']."' WHERE idAuteur=".$option['idAuteur'].";
                ";
            break;
    }
    // connexion à la base de données :
    $dsn = 'mysql:dbname='.database.';host='.hostname.';charset=utf8';
    // création d'une instance d'un objet PDO de nom $bdd
    $db = new PDO($dsn, username, password);
    // pour afficher les erreurs
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // pour récupérer le résultat des requêtes SELECT sous forme de tableaux associatifs
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_ASSOC);
    // execution de la commande :
    return $db->exec($cmd);
}

/**
 * @param $idQuery
 * @param $option
 * @return int
 */
function delete($idQuery,$option) {
    switch ($idQuery) {
        case 1:
            $cmd = "
                DELETE FROM AUTEUR WHERE AUTEUR.idAuteur='".$option['idAuteur']."';
                ";
            break;
    }
    // connexion à la base de données :
    $dsn = 'mysql:dbname='.database.';host='.hostname.';charset=utf8';
    // création d'une instance d'un objet PDO de nom $bdd
    $db = new PDO($dsn, username, password);
    // pour afficher les erreurs
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // pour récupérer le résultat des requêtes SELECT sous forme de tableaux associatifs
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_ASSOC);
    // execution de la commande :
    return $db->exec($cmd);
}

