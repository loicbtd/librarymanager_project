<?php

include 'resource/conf/db.php';

/**
 * @param $idQuery
 * @param $option
 * @return array
 */
function select($idQuery,$option) {
    switch ($idQuery) {
        case 1:
            $cmd = "
                 SELECT ADHERENT.idAdherent, ADHERENT.nomAdherent, ADHERENT.adresse, ADHERENT.email, ADHERENT.datePaiement
                , COUNT(EMPRUNT.idAdherent) as nbEmpruntEnCours
                , IF(CURRENT_DATE()>=DATE_ADD(datePaiement, INTERVAL 11 MONTH) AND CURRENT_DATE()<DATE_ADD(datePaiement, INTERVAL 1 YEAR) ,1,0) AS flagRetardProche
                , IF(CURRENT_DATE()>=DATE_ADD(datePaiement, INTERVAL 1 YEAR),1,0) AS flagRetard
                , DATE_ADD(datePaiement, INTERVAL 1 YEAR) dateButoir
                FROM ADHERENT
                LEFT JOIN EMPRUNT ON ADHERENT.idAdherent = EMPRUNT.idAdherent
                  AND EMPRUNT.dateRendu IS NULL
                GROUP BY ADHERENT.idAdherent
                ORDER BY ADHERENT.nomAdherent;
                ";
            break;

        case 2:
            $cmd = "
                SELECT COUNT(noExemplaire) AS nbEmprunt
                FROM EMPRUNT
                INNER JOIN ADHERENT ON EMPRUNT.idAdherent = ADHERENT.idAdherent
                WHERE EMPRUNT.dateRendu IS NULL
                AND ADHERENT.idAdherent=".$option['idAdherent'].";
                ";
            break;
    }
    // connexion à la base de données :
    $dsn = 'mysql:dbname='.database.';host='.hostname.';charset=utf8';
    // création d'une instance d'un objet PDO de nom $bdd
    $db = new PDO($dsn, username, password);
    // pour afficher les erreurs
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // pour récupérer le résultat des requêtes SELECT sous forme de tableaux associatifs
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_ASSOC);
    // execution de la commande :
    $answer = $db->query($cmd);
    // recupération des données sous la forme d'un tableau associatif :
    return $answer->fetchAll();
}

/**
 * @param $idQuery
 * @param $option
 * @return int
 */
function insert($idQuery,$option) {
    switch ($idQuery) {
        case 1:
            $cmd = "
                INSERT INTO ADHERENT VALUES (NULL,'".$option['nomAdherent']."','".$option['adresse']."','".$option['datePaiement']."','".$option['email']."');
                ";
            break;
    }
    // connexion à la base de données :
    $dsn = 'mysql:dbname='.database.';host='.hostname.';charset=utf8';
    // création d'une instance d'un objet PDO de nom $bdd
    $db = new PDO($dsn, username, password);
    // pour afficher les erreurs
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // pour récupérer le résultat des requêtes SELECT sous forme de tableaux associatifs
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_ASSOC);
    // execution de la commande :
    return $db->exec($cmd);
}

/**
 * @param $idQuery
 * @param $option
 * @return int
 */
function update($idQuery,$option) {
    switch ($idQuery) {
        case 1:
            $cmd = "
                UPDATE ADHERENT SET nomAdherent='".$option['nomAdherent']."', adresse='".$option['adresse']."', datePaiement='".$option['datePaiement']."', email='".$option['email']."' WHERE idAdherent=".$option['idAdherent'].";
                ";
            break;
    }
    // connexion à la base de données :
    $dsn = 'mysql:dbname='.database.';host='.hostname.';charset=utf8';
    // création d'une instance d'un objet PDO de nom $bdd
    $db = new PDO($dsn, username, password);
    // pour afficher les erreurs
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // pour récupérer le résultat des requêtes SELECT sous forme de tableaux associatifs
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_ASSOC);
    // execution de la commande :
    return $db->exec($cmd);
}

/**
 * @param $idQuery
 * @param $option
 * @return int
 */
function delete($idQuery,$option) {
    switch ($idQuery) {
        case 1:
            $cmd = "
                DELETE FROM ADHERENT WHERE idAdherent='".$option['idAdherent']."';
                ";
            break;
    }
    // connexion à la base de données :
    $dsn = 'mysql:dbname='.database.';host='.hostname.';charset=utf8';
    // création d'une instance d'un objet PDO de nom $bdd
    $db = new PDO($dsn, username, password);
    // pour afficher les erreurs
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // pour récupérer le résultat des requêtes SELECT sous forme de tableaux associatifs
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_ASSOC);
    // execution de la commande :
    return $db->exec($cmd);
}



