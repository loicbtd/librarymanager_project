<?php

include 'resource/conf/db.php';

/**
 * @param $idQuery
 * @param $option
 * @return array
 */
function select($idQuery,$option) {
    if ( isset($option['idAdherentCur']) ) {
        $option['idAdherent'] = $option['idAdherentCur'];
    }
    switch ($idQuery) {
        case 1:
            $cmd = "
                SELECT idAdherent, nomAdherent
                FROM ADHERENT
                ORDER BY nomAdherent;
                ";
            break;

        case 2:
            $cmd = "
                SELECT idAdherent, nomAdherent
                FROM ADHERENT
                WHERE idAdherent=".$option['idAdherent'].";
                ";
            break;

        case 3:
            $cmd = "
                SELECT COUNT(EXEMPLAIRE.noExemplaire) as nbEmpruntNotReturned
                FROM EMPRUNT
                INNER JOIN ADHERENT ON EMPRUNT.idAdherent = ADHERENT.idAdherent
                INNER JOIN EXEMPLAIRE ON EMPRUNT.noExemplaire = EXEMPLAIRE.noExemplaire
                INNER JOIN OEUVRE ON EXEMPLAIRE.noOeuvre = OEUVRE.noOeuvre
                INNER JOIN AUTEUR ON OEUVRE.idAuteur = AUTEUR.idAuteur
                WHERE EMPRUNT.dateRendu IS NULL AND ADHERENT.idAdherent=".$option['idAdherent'].";
                ";
            break;

        case 4:
            $cmd = "
                SELECT ADHERENT.idAdherent 
                , ADHERENT.nomAdherent
                , OEUVRE.titre
                , EMPRUNT.dateEmprunt
                , DATEDIFF( CURDATE(), EMPRUNT.dateEmprunt)  AS nbJour
                , EMPRUNT.noExemplaire
                , EMPRUNT.idAdherent
                FROM EMPRUNT
                INNER JOIN ADHERENT ON EMPRUNT.idAdherent = ADHERENT.idAdherent
                INNER JOIN EXEMPLAIRE ON EMPRUNT.noExemplaire = EXEMPLAIRE.noExemplaire
                INNER JOIN OEUVRE ON EXEMPLAIRE.noOeuvre = OEUVRE.noOeuvre
                INNER JOIN AUTEUR ON OEUVRE.idAuteur = AUTEUR.idAuteur
                WHERE EMPRUNT.dateRendu IS NULL AND ADHERENT.idAdherent=".$option['idAdherent']."
                ORDER BY EMPRUNT.dateEmprunt DESC;
                ";
            break;

        case 5:
            $cmd = "
                SELECT AUTEUR.nomAuteur, OEUVRE.titre, E2.noExemplaire
                FROM EXEMPLAIRE E1
                LEFT JOIN EXEMPLAIRE E2 ON E2.noExemplaire = E1.noExemplaire
                  AND E2.noExemplaire NOT IN (
                    SELECT EMPRUNT.noExemplaire
                    FROM EMPRUNT
                    WHERE EMPRUNT.dateRendu IS NULL
                    )
                INNER JOIN OEUVRE ON E2.noOeuvre = OEUVRE.noOeuvre
                INNER JOIN AUTEUR ON OEUVRE.idAuteur = AUTEUR.idAuteur;
                ";
            break;

        case 6:
            $cmd = "
                SELECT EXEMPLAIRE.noExemplaire
                FROM EXEMPLAIRE
                WHERE EXEMPLAIRE.noExemplaire=".$option['noExemplaire'].";
                ";
            break;

        case 7:
            $cmd = "
                SELECT COUNT(E2.noExemplaire) AS nbExemplaireDispo
                FROM EXEMPLAIRE E1
                LEFT JOIN EXEMPLAIRE E2 ON E2.noExemplaire = E1.noExemplaire
                  AND E2.noExemplaire NOT IN (
                    SELECT EMPRUNT.noExemplaire
                    FROM EMPRUNT
                    WHERE EMPRUNT.dateRendu IS NULL
                    )
                INNER JOIN OEUVRE ON E2.noOeuvre = OEUVRE.noOeuvre
                INNER JOIN AUTEUR ON OEUVRE.idAuteur = AUTEUR.idAuteur;
                ";
            break;

        case 8:
            $cmd = "
                SELECT IF(CURRENT_DATE()>=DATE_ADD(datePaiement, INTERVAL 1 YEAR),1,0) AS flagRetard
                FROM ADHERENT
                WHERE idAdherent=".$option['idAdherent'].";
                ";
            break;

        case 9:
            $cmd = "
                SELECT ADHERENT.idAdherent
                , ADHERENT.nomAdherent
                , OEUVRE.titre
                , EMPRUNT.dateEmprunt
                , IF(EMPRUNT.dateRendu IS NULL,'',EMPRUNT.dateRendu) AS dateRendu
                , EMPRUNT.noExemplaire
                , IF(EMPRUNT.dateRendu IS NULL,DATEDIFF(CURDATE(),EMPRUNT.dateEmprunt),DATEDIFF(EMPRUNT.dateRendu,EMPRUNT.dateEmprunt)) AS nbJour
                FROM EMPRUNT
                INNER JOIN ADHERENT ON EMPRUNT.idAdherent = ADHERENT.idAdherent
                INNER JOIN EXEMPLAIRE ON EMPRUNT.noExemplaire = EXEMPLAIRE.noExemplaire
                INNER JOIN OEUVRE ON EXEMPLAIRE.noOeuvre = OEUVRE.noOeuvre
                ORDER BY ADHERENT.nomAdherent ASC, EMPRUNT.dateEmprunt DESC;
                ";
            break;

        case 10:
            $cmd = "
                SELECT ADHERENT.idAdherent
                , ADHERENT.nomAdherent
                , OEUVRE.titre
                , EMPRUNT.dateEmprunt
                , IF(EMPRUNT.dateRendu IS NULL,'',EMPRUNT.dateRendu) AS dateRendu
                , EMPRUNT.noExemplaire
                , IF(EMPRUNT.dateRendu IS NULL,DATEDIFF(CURDATE(),EMPRUNT.dateEmprunt),DATEDIFF(EMPRUNT.dateRendu,EMPRUNT.dateEmprunt)) AS nbJour
                , EMPRUNT.noExemplaire
                FROM EMPRUNT
                INNER JOIN ADHERENT ON EMPRUNT.idAdherent = ADHERENT.idAdherent
                INNER JOIN EXEMPLAIRE ON EMPRUNT.noExemplaire = EXEMPLAIRE.noExemplaire
                INNER JOIN OEUVRE ON EXEMPLAIRE.noOeuvre = OEUVRE.noOeuvre
                WHERE EMPRUNT.idAdherent=".$option['idAdherent']."
                ORDER BY EMPRUNT.dateEmprunt DESC;
                ";
            break;

        case 11:
            $cmd = "
                SELECT ADHERENT.idAdherent
                , EMPRUNT.noExemplaire
                , OEUVRE.titre
                , ADHERENT.nomAdherent
                , EMPRUNT.dateEmprunt
                , EMPRUNT.dateRendu
                , ADHERENT.email
                , DATEDIFF( CURDATE(), EMPRUNT.dateEmprunt ) AS nbJoursEmprunt
                , DATEDIFF( CURDATE(), DATE_ADD(EMPRUNT.dateEmprunt, INTERVAL 90 DAY) ) AS retard
                , DATE_ADD(EMPRUNT.dateEmprunt, INTERVAL 90 DAY) AS dateRenduTheorique
                , IF( CURRENT_DATE() > DATE_ADD(EMPRUNT.dateEmprunt, INTERVAL 90 DAY),1,0) AS flagRetard
                , IF( CURRENT_DATE() > DATE_ADD(EMPRUNT.dateEmprunt, INTERVAL 120 DAY),1,0) AS flagPenalite
                , IF( DATEDIFF( CURDATE(), EMPRUNT.dateEmprunt) > 120
                    , IF( (DATEDIFF( CURDATE(), EMPRUNT.dateEmprunt)-120)*0.5 > 25
                      , 25
                      , (DATEDIFF(CURDATE(), EMPRUNT.dateEmprunt)-120)*0.5
                      )
                    , 0
                  ) AS dette
                FROM EMPRUNT
                INNER JOIN ADHERENT ON EMPRUNT.idAdherent = ADHERENT.idAdherent
                INNER JOIN EXEMPLAIRE ON EMPRUNT.noExemplaire = EXEMPLAIRE.noExemplaire
                INNER JOIN OEUVRE ON EXEMPLAIRE.noOeuvre = OEUVRE.noOeuvre
                INNER JOIN AUTEUR ON OEUVRE.idAuteur = AUTEUR.idAuteur
                WHERE EMPRUNT.dateRendu IS NULL
                    AND DATEDIFF( CURDATE(), DATE_ADD(EMPRUNT.dateEmprunt, INTERVAL 90 DAY) ) > 0
                ORDER BY EMPRUNT.dateEmprunt;
                ";
            break;

        case 12:
            $cmd = "
                SELECT idAdherent, nomAdherent
                FROM ADHERENT
                WHERE CURRENT_DATE()<DATE_ADD(datePaiement, INTERVAL 1 YEAR)
                    AND ADHERENT.idAdherent NOT IN (
                        SELECT DISTINCT idAdherent
                        FROM EMPRUNT
                        WHERE dateRendu IS NULL
                        GROUP BY idAdherent
                        HAVING COUNT(noExemplaire)>=5
                    )
                ORDER BY idAdherent;
                ";
            break;
        case 13:
            $cmd = "
                SELECT ADHERENT.idAdherent
                , ADHERENT.nomAdherent
                , OEUVRE.titre
                , EMPRUNT.dateEmprunt
                , DATEDIFF( CURDATE(), EMPRUNT.dateEmprunt)  AS nbJour
                , EMPRUNT.noExemplaire
                , ADHERENT.idAdherent
                FROM EMPRUNT
                INNER JOIN ADHERENT ON EMPRUNT.idAdherent = ADHERENT.idAdherent
                INNER JOIN EXEMPLAIRE ON EMPRUNT.noExemplaire = EXEMPLAIRE.noExemplaire
                INNER JOIN OEUVRE ON EXEMPLAIRE.noOeuvre = OEUVRE.noOeuvre
                INNER JOIN AUTEUR ON OEUVRE.idAuteur = AUTEUR.idAuteur
                WHERE EMPRUNT.dateRendu IS NULL
                ORDER BY EMPRUNT.dateEmprunt DESC;
                ";
            break;
        case 14:
            $cmd = "
                SELECT DISTINCT ADHERENT.idAdherent, ADHERENT.nomAdherent
                FROM EMPRUNT
                INNER JOIN ADHERENT ON EMPRUNT.idAdherent = ADHERENT.idAdherent
                WHERE EMPRUNT.dateRendu IS NULL
                ORDER BY nomAdherent;
                ";
            break;

    }
    // connexion à la base de données :
    $dsn = 'mysql:dbname='.database.';host='.hostname.';charset=utf8';
    // création d'une instance d'un objet PDO de nom $bdd
    $db = new PDO($dsn, username, password);
    // pour afficher les erreurs
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // pour récupérer le résultat des requêtes SELECT sous forme de tableaux associatifs
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_ASSOC);
    // execution de la commande :
    $answer = $db->query($cmd);
    // recupération des données sous la forme d'un tableau associatif :
    return $answer->fetchAll();
}

/**
 * @param $idQuery
 * @param $option
 * @return int
 */
function insert($idQuery,$option) {
    switch ($idQuery) {
        case 1:
            $cmd = "
                INSERT INTO EMPRUNT VALUES (".$option['idAdherent'].",".$option['noExemplaire'].",'".$option['dateEmprunt']."',NULL);
                ";
            break;
    }
    // connexion à la base de données :
    $dsn = 'mysql:dbname='.database.';host='.hostname.';charset=utf8';
    // création d'une instance d'un objet PDO de nom $bdd
    $db = new PDO($dsn, username, password);
    // pour afficher les erreurs
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // pour récupérer le résultat des requêtes SELECT sous forme de tableaux associatifs
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_ASSOC);
    // execution de la commande :
    return $db->exec($cmd);
}

/**
 * @param $idQuery
 * @param $option
 * @return int
 */
function update($idQuery,$option) {
    switch ($idQuery) {
        case 1:
            $cmd = "
                UPDATE EXEMPLAIRE SET etat='".$option['etat']."', dateAchat='".$option['dateAchat']."', prix=".$option['prix']." WHERE noExemplaire=".$option['noExemplaire'].";
                ";
            break;
        case 2:
            $cmd = "
                UPDATE EMPRUNT SET dateRendu = '".$option['dateRendu']."'
                WHERE idAdherent=".$option['idAdherent']." AND dateEmprunt='".$option['dateEmprunt']."' AND noExemplaire=".$option['noExemplaire'].";
                ";
            break;
    }
    // connexion à la base de données :
    $dsn = 'mysql:dbname='.database.';host='.hostname.';charset=utf8';
    // création d'une instance d'un objet PDO de nom $bdd
    $db = new PDO($dsn, username, password);
    // pour afficher les erreurs
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // pour récupérer le résultat des requêtes SELECT sous forme de tableaux associatifs
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_ASSOC);
    // execution de la commande :
    return $db->exec($cmd);
}

/**
 * @param $idQuery
 * @param $option
 * @return int
 */
function delete($idQuery,$option) {
    switch ($idQuery) {
        case 1:
            $cmd = "
                DELETE FROM EMPRUNT 
                WHERE idAdherent=".$option['idAdherent']."
                    AND dateEmprunt='".$option['dateEmprunt']."'
                    AND noExemplaire=".$option['noExemplaire'].";
                ";
            break;
    }
    // connexion à la base de données :
    $dsn = 'mysql:dbname='.database.';host='.hostname.';charset=utf8';
    // création d'une instance d'un objet PDO de nom $bdd
    $db = new PDO($dsn, username, password);
    // pour afficher les erreurs
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // pour récupérer le résultat des requêtes SELECT sous forme de tableaux associatifs
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_ASSOC);
    // execution de la commande :
    return $db->exec($cmd);
}