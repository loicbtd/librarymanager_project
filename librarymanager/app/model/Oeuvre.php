<?php

include 'resource/conf/db.php';

/**
 * @param $idQuery
 * @param $option
 * @return array
 */
function select($idQuery,$option) {
    switch ($idQuery) {
        case 1:
            $cmd = "
                SELECT AUTEUR.nomAuteur
                , OEUVRE.titre
                , IFNULL(OEUVRE.dateParution,'') as dateParution
                , COUNT(E1.noExemplaire) AS nbTotal
                , COUNT(E2.noExemplaire) AS nbDispo
                , OEUVRE.noOeuvre
                , OEUVRE.idAuteur
                FROM OEUVRE
                INNER JOIN AUTEUR ON OEUVRE.idAuteur = AUTEUR.idAuteur
                LEFT JOIN EXEMPLAIRE E1 ON OEUVRE.noOeuvre = E1.noOeuvre
                LEFT JOIN EXEMPLAIRE E2 ON E1.noExemplaire = E2.noExemplaire
                  AND E2.noExemplaire NOT IN (
                    SELECT EMPRUNT.noExemplaire
                    FROM EMPRUNT
                    WHERE EMPRUNT.dateRendu IS NULL)
                GROUP BY OEUVRE.noOeuvre
                ORDER BY AUTEUR.nomAuteur ASC, OEUVRE.titre ASC;
                ";
            break;

        case 2:
            $cmd = "
                SELECT idAuteur, nomAuteur, prenomAuteur FROM AUTEUR ORDER BY nomAuteur;
                ";
            break;

        case 3:
            $cmd = "
                SELECT COUNT(EXEMPLAIRE.noExemplaire) AS nbExemplaire
                FROM EXEMPLAIRE
                RIGHT JOIN OEUVRE ON EXEMPLAIRE.noOeuvre = OEUVRE.noOeuvre AND OEUVRE.noOeuvre=".$option['noOeuvre'].";
                ";
            break;

        case 4:
            $cmd = "
                SELECT EXEMPLAIRE.noExemplaire
                , EXEMPLAIRE.etat
                , EXEMPLAIRE.dateAchat
                , EXEMPLAIRE.prix
                FROM EXEMPLAIRE
                WHERE EXEMPLAIRE.noOeuvre = ".$option['noOeuvre'].";
                ";
            break;

    }
    // connexion à la base de données :
    $dsn = 'mysql:dbname='.database.';host='.hostname.';charset=utf8';
    // création d'une instance d'un objet PDO de nom $bdd
    $db = new PDO($dsn, username, password);
    // pour afficher les erreurs
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // pour récupérer le résultat des requêtes SELECT sous forme de tableaux associatifs
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_ASSOC);
    // execution de la commande :
    $answer = $db->query($cmd);
    // recupération des données sous la forme d'un tableau associatif :
    return $answer->fetchAll();
}

/**
 * @param $idQuery
 * @param $option
 * @return int
 */
function insert($idQuery,$option) {
    switch ($idQuery) {
        case 1:
            $cmd = "
                INSERT INTO OEUVRE VALUES (NULL,'".$option['titre']."','".$option['dateParution']."',".$option['idAuteur'].");
                ";
            break;
    }
    // connexion à la base de données :
    $dsn = 'mysql:dbname='.database.';host='.hostname.';charset=utf8';
    // création d'une instance d'un objet PDO de nom $bdd
    $db = new PDO($dsn, username, password);
    // pour afficher les erreurs
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // pour récupérer le résultat des requêtes SELECT sous forme de tableaux associatifs
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_ASSOC);
    // execution de la commande :
    return $db->exec($cmd);
}

/**
 * @param $idQuery
 * @param $option
 * @return int
 */
function update($idQuery,$option) {
    switch ($idQuery) {
        case 1:
            $cmd = "
                UPDATE OEUVRE 
                SET titre='".$option['titre']."'
                , dateParution='".$option['dateParution']."'
                , idAuteur=".$option['idAuteur']."
                WHERE noOeuvre=".$option['noOeuvre'].";
                ";
            break;
    }
    // connexion à la base de données :
    $dsn = 'mysql:dbname='.database.';host='.hostname.';charset=utf8';
    // création d'une instance d'un objet PDO de nom $bdd
    $db = new PDO($dsn, username, password);
    // pour afficher les erreurs
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // pour récupérer le résultat des requêtes SELECT sous forme de tableaux associatifs
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_ASSOC);
    // execution de la commande :
    return $db->exec($cmd);
}

/**
 * @param $idQuery
 * @param $option
 * @return int
 */
function delete($idQuery,$option) {
    switch ($idQuery) {
        case 1:
            $cmd = "
                DELETE FROM OEUVRE WHERE noOeuvre=".$option['noOeuvre'].";
                ";
            break;
    }
    // connexion à la base de données :
    $dsn = 'mysql:dbname='.database.';host='.hostname.';charset=utf8';
    // création d'une instance d'un objet PDO de nom $bdd
    $db = new PDO($dsn, username, password);
    // pour afficher les erreurs
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // pour récupérer le résultat des requêtes SELECT sous forme de tableaux associatifs
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_ASSOC);
    // execution de la commande :
    return $db->exec($cmd);
}