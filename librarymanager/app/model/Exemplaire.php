<?php

include 'resource/conf/db.php';

/**
 * @param $idQuery
 * @param $option
 * @return array
 */
function select($idQuery,$option) {
    switch ($idQuery) {
        case 1:
            $cmd = "
                SELECT E1.noExemplaire
                , IF(E2.noExemplaire IS NULL,0,1) AS flagPresent
                , E1.etat
                , E1.dateAchat
                , E1.prix
                FROM OEUVRE
                LEFT JOIN EXEMPLAIRE E1 ON E1.noOeuvre = OEUVRE.noOeuvre
                LEFT JOIN EXEMPLAIRE E2 ON E2.noExemplaire = E1.noExemplaire
                  AND E2.noExemplaire NOT IN (
                    SELECT EMPRUNT.noExemplaire
                    FROM EMPRUNT
                    WHERE EMPRUNT.dateRendu IS NULL)
                  WHERE OEUVRE.noOeuvre = ".$option['noOeuvre']."
                ORDER BY E1.noExemplaire ASC;
                ";
            break;

        case 2:
            $cmd = "
                SELECT AUTEUR.nomAuteur
                , AUTEUR.prenomAuteur
                , OEUVRE.dateParution
                , OEUVRE.titre
                , COUNT(E1.noExemplaire) AS nbTotal
                , COUNT(E2.noExemplaire) AS nbDispo
                FROM OEUVRE
                INNER JOIN AUTEUR ON OEUVRE.idAuteur = AUTEUR.idAuteur
                LEFT JOIN EXEMPLAIRE E1 ON OEUVRE.noOeuvre = E1.noOeuvre
                LEFT JOIN EXEMPLAIRE E2 ON E1.noExemplaire = E2.noExemplaire
                  AND E2.noExemplaire NOT IN (
                    SELECT EMPRUNT.noExemplaire
                    FROM EMPRUNT
                    WHERE EMPRUNT.dateRendu IS NULL)
                WHERE OEUVRE.noOeuvre=".$option['noOeuvre'].";
                ";
            break;

        case 3:
            $cmd = "
                SELECT COUNT(EXEMPLAIRE.noExemplaire) AS nbExemplaire
                FROM EXEMPLAIRE
                RIGHT JOIN OEUVRE ON EXEMPLAIRE.noOeuvre = OEUVRE.noOeuvre AND OEUVRE.noOeuvre=".$option['noOeuvre'].";
                ";
            break;

        case 4:
            $cmd = "
                SELECT COUNT(noExemplaire) AS nbEmprunt
                FROM EMPRUNT
                WHERE noExemplaire=".$option['noExemplaire'].";
                ";
            break;
    }
    // connexion à la base de données :
    $dsn = 'mysql:dbname='.database.';host='.hostname.';charset=utf8';
    // création d'une instance d'un objet PDO de nom $bdd
    $db = new PDO($dsn, username, password);
    // pour afficher les erreurs
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // pour récupérer le résultat des requêtes SELECT sous forme de tableaux associatifs
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_ASSOC);
    // execution de la commande :
    $answer = $db->query($cmd);
    // recupération des données sous la forme d'un tableau associatif :
    return $answer->fetchAll();
}

/**
 * @param $idQuery
 * @param $option
 * @return int
 */
function insert($idQuery,$option) {
    switch ($idQuery) {
        case 1:
            $cmd = "
                INSERT INTO EXEMPLAIRE VALUES (NULL,'".$option['etat']."','".$option['dateAchat']."',".$option['prix'].",".$option['noOeuvre'].");
                ";
            break;
    }
    // connexion à la base de données :
    $dsn = 'mysql:dbname='.database.';host='.hostname.';charset=utf8';
    // création d'une instance d'un objet PDO de nom $bdd
    $db = new PDO($dsn, username, password);
    // pour afficher les erreurs
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // pour récupérer le résultat des requêtes SELECT sous forme de tableaux associatifs
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_ASSOC);
    // execution de la commande :
    return $db->exec($cmd);
}

/**
 * @param $idQuery
 * @param $option
 * @return int
 */
function update($idQuery,$option) {
    switch ($idQuery) {
        case 1:
            $cmd = "
                UPDATE EXEMPLAIRE SET etat='".$option['etat']."', dateAchat='".$option['dateAchat']."', prix=".$option['prix']." WHERE noExemplaire=".$option['noExemplaire'].";
                ";
            break;
    }
    // connexion à la base de données :
    $dsn = 'mysql:dbname='.database.';host='.hostname.';charset=utf8';
    // création d'une instance d'un objet PDO de nom $bdd
    $db = new PDO($dsn, username, password);
    // pour afficher les erreurs
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // pour récupérer le résultat des requêtes SELECT sous forme de tableaux associatifs
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_ASSOC);
    // execution de la commande :
    return $db->exec($cmd);
}

/**
 * @param $idQuery
 * @param $option
 * @return int
 */
function delete($idQuery,$option) {
    switch ($idQuery) {
        case 1:
            $cmd = "
                DELETE FROM EXEMPLAIRE WHERE noExemplaire=".$option['noExemplaire'].";
                ";
            break;
    }
    // connexion à la base de données :
    $dsn = 'mysql:dbname='.database.';host='.hostname.';charset=utf8';
    // création d'une instance d'un objet PDO de nom $bdd
    $db = new PDO($dsn, username, password);
    // pour afficher les erreurs
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    // pour récupérer le résultat des requêtes SELECT sous forme de tableaux associatifs
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_ASSOC);
    // execution de la commande :
    return $db->exec($cmd);
}