<?php

function isNameValid($strText) {
    return preg_match('/^[A-Za-zàâäéèêëîïôöùûüÀÂÄÉÈÊËÎÏÔÖÙÛÜ[:punct:]]{2}+[A-Za-zàâäéèêëîïôöùûüÀÂÄÉÈÊËÎÏÔÖÙÛÜ[:space:][:punct:]]*$/', $strText);
}

function isEmailValid($strText) {
    return preg_match('/^\S+[@]+\S+[.]+\S+$/', $strText);
}

function setTextError($idError) {
    switch ($idError) {
        case "email": return "l'email doit être au format x@x.x";
        case "title": return "le titre doit comporter 2 caractères au minimum";
        case "name": return "le nom doit comporter 2 caractères au minimum";
        case "address": return "l'adresse doit comporter 2 caractères au minimum";
        default: return "undefined error";
    }
}

function oldparseEmpruntKeys($strIn) {
    $pos = 0;
    $strTmp = "";
    for ($i = 0; $i < strlen($strIn); $i++) {
        if ($strIn[$i] != " " ) {
            $strTmp = $strTmp.$strIn[$i];
        }
        else {
            switch ($pos) {
                case 0:
                    $idAdherent = $strTmp;
                    break;

                case 2:
                    $dateEmprunt = $strTmp;
                    break;

                case 3:
                    $noExemplaire = $strTmp;
                    break;
            }
            $i++;
            $pos++;
            $strTmp = "";
        }
    }

    return array (
        "idAdherent" => $idAdherent
        ,"dateEmprunt" => $dateEmprunt
        ,"noExemplaire" => $noExemplaire
    );
}


function parseEmpruntKeys($strIn) {
    return str_getcsv($strIn, ",");
}