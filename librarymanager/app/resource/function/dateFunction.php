<?php

function testDate($strDate) {

    if ($strDate == "") { return -10; }

    if (!preg_match("#^([0-9]{1,2})/([0-9]{1,2})/([0-9]{4})$#", $strDate) ) { return -1; }

    $arrDate = date_parse_from_format("d/m/Y", $strDate);
    switch ($arrDate['month']) {
        case 1:
            if ($arrDate['day'] < 1|| $arrDate['day'] > 31) { return 1; }
            else { return 0; }

        case 2:
            if ( ($arrDate['year']%4==0) && ( ($arrDate['year']%100!=0) || ($arrDate['year']%400==0) ) ) {
                if ($arrDate['day'] < 1|| $arrDate['day'] > 29) { return -2; }
                else { return 0; }
            }
            else {
                if ($arrDate['day'] < 1|| $arrDate['day'] > 28) { return 2; }
                else { return 0; }
            }

        case 3:
            if ($arrDate['day'] < 1|| $arrDate['day'] > 31) { return 3; }
            else { return 0; }

        case 4:
            if ($arrDate['day'] < 1|| $arrDate['day'] > 30) { return 4; }
            else { return 0; }

        case 5:
            if ($arrDate['day'] < 1|| $arrDate['day'] > 31) { return 5; }
            else { return 0; }

        case 6:
            if ($arrDate['day'] < 1|| $arrDate['day'] > 30) { return 6; }
            else { return 0; }

        case 7:
            if ($arrDate['day'] < 1|| $arrDate['day'] > 31) { return 7; }
            else { return 0; }

        case 8:
            if ($arrDate['day'] < 1|| $arrDate['day'] > 31) { return 8; }
            else { return 0; }

        case 9:
            if ($arrDate['day'] < 1|| $arrDate['day'] > 30) { return 9; }
            else { return 0; }

        case 10:
            if ($arrDate['day'] < 1|| $arrDate['day'] > 31) { return 10; }
            else { return 0; }

        case 11:
            if ($arrDate['day'] < 1|| $arrDate['day'] > 30) { return 11; }
            else { return 0; }

        case 12:
            if ($arrDate['day'] < 1|| $arrDate['day'] > 31) { return 12; }
            else { return 0; }

        default: return 13;
    }
}

function setDateError($idError) {
    switch ($idError) {
        case -10: return "la date doit être renseignée";
        case -2: return "jour incorrect, cette année, février 1 < jour < 29";
        case -1: return "la date doit être au format jj/mm/aaaa";
        case 1: return "jour incorrect, janvier: 1 < jour < 31";
        case 2: return "jour incorrect, cette année, février: 1 < jour < 28";
        case 3: return "jour incorrect, mars: 1 < jour < 31";
        case 4: return "jour incorrect, avril: 1 < jour < 30";
        case 5: return "jour incorrect, mai: 1 < jour < 31";
        case 6: return "jour incorrect, juin: 1 < jour < 30";
        case 7: return "jour incorrect, juillet: 1 < jour < 31";
        case 8: return "jour incorrect, août: 1 < jour < 31";
        case 9: return "jour incorrect, septembre: 1 < jour < 30";
        case 10: return "jour incorrect, octobre: 1 < jour < 31";
        case 11: return "jour incorrect, novembre: 1 < jour < 30";
        case 12: return "jour incorrect, décembre: 1 < jour < 31";
        case 13: return "mois incorrect, 1 < mois < 12";
        default: return "undefined error";
    }
}

function dateFormatSql($strDate) {
    return date("Y-m-d",strtotime(str_replace('/','-',$strDate)));
}

function dateFormatDisplay($strDate) {
    if ($strDate == "") return "";
    return date("d/m/Y", strtotime($strDate));
}