<?php

function testPrice($strPrice) {

    if ($strPrice == "") { return 1; }

    if ( !preg_match('/^[[:digit:]]{1,}$|^[[:digit:]]{1,},[[:digit:]]{1,2}$/', $strPrice) ) { return 2; }

    return 0;
}

function setPriceError($idError) {
    switch ($idError) {
        case 1: return "le prix doit être renseigné";
        case 2: return "le prix doit être au format dd,dd";
        default: return "undefined error";
    }
}

function priceFormatSql($strPrice) {
    return preg_replace('/,/', '.', $strPrice);
}

function priceFormatDisplay($strPrice) {
    return str_replace('.',',',$strPrice);
}

