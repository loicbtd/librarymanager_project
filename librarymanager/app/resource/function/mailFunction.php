<?php

require 'resource/phpmailer/src/PHPMailer.php';
require 'resource/phpmailer/src/SMTP.php';
require 'resource/phpmailer/src/Exception.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;


function sendMail($sendToEmail, $sendToName, $titre, $retard) {
    $mail = new PHPMailer(true);

    try {
        //Server settings
        $mail->SMTPDebug = 0;                                       // Enable verbose debug output
        $mail->CharSet = 'UTF-8';
        $mail->isSMTP();                                            // Set mailer to use SMTP
        $mail->Host       = 'smtp.gmail.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        $mail->Username   = 'manager.librarymanagerproject@gmail.com';                     // SMTP username
        $mail->Password   = 'librarymanager@*';                               // SMTP password
        $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
        $mail->Port       = 587;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom('manager.librarymanagerproject@gmail.com', 'Librairie Belfort');
        $mail->addReplyTo('noreply.manager.librarymanagerproject@gmail.com', 'no reply');
        $mail->addAddress($sendToEmail, $sendToName);

        // Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Retard de restitution';
        $mail->Body    =
            "
            Bonjour Mme/M. ".$sendToName.",<br><br>
            Nous vous contactons car vous etes en retard de ".$retard." jours dans la restitution d'un exemplaire de ".$titre.".<br><br>
            Bien cordialement,<br><br>
            Service d'emprunt de documents<br>
            Librairie de Belfort
            ";

        $mail->AltBody = "Bonjour Mme/M. ".$sendToName.", Nous vous contactons car vous etes en retard de ".$retard." jours dans la restitution d'un exemplaire de ".$titre.". Bien cordialement, Service d'emprunt de documents, Librairie de Belfort";

        $mail->send();
        return true;
    } catch (Exception $e) {
        return false;
    }
}

