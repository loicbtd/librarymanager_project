<nav class="navbar sticky-top navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="/">
        <img src="resource/image/icon_bookshelf.svg" width="30" height="30" class="d-inline-block align-top" alt="">
        LibraryManager
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item active dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Gestion des emprunts
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="/EmpruntReturn">Retourner des emprunts</a>
                    <a class="dropdown-item" href="/EmpruntAdd">Ajouter des emprunts</a>
                    <a class="dropdown-item" href="/EmpruntDel">Supprimer des emprunts</a>
                    <a class="dropdown-item" href="/EmpruntBilan">Consulter les retards</a>
                </div>
            </li>
            <li class="nav-item active dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Gestion des oeuvres et auteurs
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="/OeuvreShow">Gestion des oeuvres et de leurs exemplaires</a>
                    <a class="dropdown-item" href="/AuteurShow">Afficher - éditer - supprimer  des auteurs</a>
                </div>
            </li>
            <li class="nav-item active dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Gestion des adhérents
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="/AdherentShow">Afficher - éditer - supprimer  des adhérents</a>
                </div>
            </li>
        </ul>
        <div class="m-1 d-inline align-content-center">
            <span class="badge badge-light text-wrap"><?php if(isset($_SESSION['name']) ) echo $_SESSION['name'];  ?></span>
        </div>
        <form class="d-inline" action="/" method="post">
            <input type="hidden" class="form-control" name="logout" value="1">
            <button title="logout" type="submit" class="btn btn-sm btn-danger m-1"><i class="fas fa-sign-out-alt"></i></button>
        </form>
    </div>
</nav>
