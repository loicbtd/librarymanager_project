<footer class="container-fluid fixed-bottom bg-dark">
    <div class="row">
        <?php if( isset($path) ): ?>
        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
            <span class="navbar-text text-white font-weight-bold">
                <?php
                    for($i = 0; $i < sizeof($path); $i++) {
                        echo $path[$i];
                        if( $i < sizeof($path)-1 ) { echo ' > '; }
                    };
                ?>
            </span>
        </div>
        <?php else: ?>
        <div class="col-12 col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 text-center">
            <span class="navbar-text text-white">
                LibraryManager - a Loïc Bertrand & Yan Imensar production
                <i class="fab fa-creative-commons"></i>
                <i class="fab fa-creative-commons-by"></i>
                <i class="fab fa-creative-commons-nc-eu"></i>
                <i class="fab fa-creative-commons-nd"></i>
            </span>
        </div>
        <?php endif; ?>
        <a id="toTheTop"><span><i class="fa fa-chevron-circle-up"></i></span></a>
    </div>
</footer>


