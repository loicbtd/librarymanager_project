<?php include 'resource/include/logPageController.php'; ?>

<style>
    body {
        margin: 0;
        padding: 0;
        background-color: #343a40;
        height: 100vh;
    }
    #login .container #login-row #login-column #login-box {
        margin-top: 20px;
        max-width: 600px;
        height: 320px;
        border: 1px solid #9C9C9C;
        background-color: #EAEAEA;
    }
    #login .container #login-row #login-column #login-box #login-form {
        padding: 20px;
    }
    #login .container #login-row #login-column #login-box #login-form #register-link {
        margin-top: -85px;
    }
</style>
<div id="login">
    <h3 class="text-center text-white pt-5">
        <img src="resource/image/icon_bookshelf.svg" width="30" height="30" alt="logo">
        LibraryManager
    </h3>
    <div class="container">
        <div id="login-row" class="row justify-content-center align-items-center">
            <div id="login-column" class="col-md-6">
                <div id="login-box" class="col-md-12">
                    <form id="login-form" class="form" action="index.php" method="post">
                        <h3 class="text-center text-info">Connexion</h3>
                        <div class="form-group">
                            <label for="username" class="text-info">Identifiant:</label><br>
                            <input type="text" name="username" id="username" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="password" class="text-info">Mot de passe:</label><br>
                            <input type="password" name="password" id="password" class="form-control">
                        </div>
                        <div class="form-group">
                            <button type="submit" name="submit" class="btn btn-info btn-md">se connecter</button>
                        </div>
                        <?php if(isset($error)): ?>
                            <small class="form-text font-weight-bold text-danger"><?=$error?></small>
                        <?php endif; ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>