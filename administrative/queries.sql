-- 1 GESTION DES EMPRUNTS
-- 1.1 EMRPUNTER DES LIVRES
-- 1.1.0 afficher les noms des adhérents
SELECT idAdherent, nomAdherent FROM ADHERENT ORDER BY nomAdherent;
-- 1.1.1 afficher tous les emprunts en cours d'un adhérent
SELECT OEUVRE.titre
, EMPRUNT.dateEmprunt
, DATEDIFF( CURDATE(), EMPRUNT.dateEmprunt)  AS nbJour
FROM EMPRUNT
INNER JOIN ADHERENT ON EMPRUNT.idAdherent = ADHERENT.idAdherent
INNER JOIN EXEMPLAIRE ON EMPRUNT.noExemplaire = EXEMPLAIRE.noExemplaire
INNER JOIN OEUVRE ON EXEMPLAIRE.noOeuvre = OEUVRE.noOeuvre
INNER JOIN AUTEUR ON OEUVRE.idAuteur = AUTEUR.idAuteur
WHERE EMPRUNT.dateRendu IS NULL AND ADHERENT.idAdherent=3
ORDER BY EMPRUNT.dateEmprunt DESC;
-- 1.1.2 afficher les exemplaires disponibles
SELECT AUTEUR.nomAuteur, OEUVRE.titre, E2.noExemplaire
FROM EXEMPLAIRE E1
LEFT JOIN EXEMPLAIRE E2 ON E2.noExemplaire = E1.noExemplaire
  AND E2.noExemplaire NOT IN (
    SELECT EMPRUNT.noExemplaire
    FROM EMPRUNT
    WHERE EMPRUNT.dateRendu IS NULL
    )
INNER JOIN OEUVRE ON E2.noOeuvre = OEUVRE.noOeuvre
INNER JOIN AUTEUR ON OEUVRE.idAuteur = AUTEUR.idAuteur;

-- 1.1.3 ajouter un exemplaire à la table emprunts
INSERT INTO EMPRUNT VALUES (2,5,'0000-00-00',NULL);
-- 1.1.4 compter le nombre d'emprunt en cours d'un adherent
SELECT COUNT(noExemplaire) AS nbEmprunt
FROM EMPRUNT
INNER JOIN ADHERENT ON EMPRUNT.idAdherent = ADHERENT.idAdherent
WHERE EMPRUNT.dateRendu IS NULL
    AND ADHERENT.idAdherent=3;
-- 1.2 RENDRE DES LIVRES
-- 1.2.1 afficher les adhérents qui ont des emprunts
SELECT DISTINCT ADHERENT.nomAdherent
FROM EMPRUNT
INNER JOIN ADHERENT ON EMPRUNT.idAdherent = ADHERENT.idAdherent
WHERE EMPRUNT.dateRendu IS NULL;

-- 1.2.2 afficher les emprunts d'un adéhrent
SELECT OEUVRE.titre
, EMPRUNT.dateEmprunt
, DATEDIFF( CURDATE(), EMPRUNT.dateEmprunt)  AS nbJour
, EXEMPLAIRE.noExemplaire
FROM EMPRUNT
INNER JOIN ADHERENT ON EMPRUNT.idAdherent = ADHERENT.idAdherent
INNER JOIN EXEMPLAIRE ON EMPRUNT.noExemplaire = EXEMPLAIRE.noExemplaire
INNER JOIN OEUVRE ON EXEMPLAIRE.noOeuvre = OEUVRE.noOeuvre
INNER JOIN AUTEUR ON OEUVRE.idAuteur = AUTEUR.idAuteur
WHERE EMPRUNT.dateRendu IS NULL AND ADHERENT.idAdherent=3
ORDER BY EMPRUNT.dateEmprunt DESC;

-- ADDED : afficher uniquement les exemplaire qui ont payé et qui ont moins de 5 emprunts en cours
SELECT idAdherent, nomAdherent
FROM ADHERENT
WHERE CURRENT_DATE()<DATE_ADD(datePaiement, INTERVAL 1 YEAR)
    AND ADHERENT.idAdherent NOT IN (
        SELECT DISTINCT idAdherent
        FROM EMPRUNT
        WHERE dateRendu IS NULL
        GROUP BY idAdherent
        HAVING COUNT(noExemplaire)>=5
    )
ORDER BY idAdherent;




-- 1.2.3 rendre le livre
UPDATE EMPRUNT SET dateRendu'0000-00-00' WHERE idAdherent=3 AND dateEmprunt='0000-00-00' AND noExemplaire=1;

-- 1.3 SUPPRIMER DES EMPRUNTS
-- 1.3.1 AFFICHER LES EMPRUNTS
-- 1.3.1.1 afficher tous les emprunts
SELECT ADHERENT.nomAdherent
, OEUVRE.titre
, EMPRUNT.dateEmprunt
, IF(EMPRUNT.dateRendu IS NULL,'',EMPRUNT.dateRendu) AS dateRendu
, EMPRUNT.noExemplaire
, IF(EMPRUNT.dateRendu IS NULL,DATEDIFF(CURDATE(),EMPRUNT.dateEmprunt),'') AS nbJour
FROM EMPRUNT
INNER JOIN ADHERENT ON EMPRUNT.idAdherent = ADHERENT.idAdherent
INNER JOIN EXEMPLAIRE ON EMPRUNT.noExemplaire = EXEMPLAIRE.noExemplaire
INNER JOIN OEUVRE ON EXEMPLAIRE.noOeuvre = OEUVRE.noOeuvre
ORDER BY EMPRUNT.dateEmprunt DESC;

-- 1.3.1.2 afficher tous les adhérents ayant des emprunts
SELECT DISTINCT ADHERENT.nomAdherent
FROM EMPRUNT
INNER JOIN ADHERENT ON EMPRUNT.idAdherent = ADHERENT.idAdherent
WHERE EMPRUNT.dateRendu IS NULL;

-- 1.3.1.2 afficher les emprunts d'un adhérent
SELECT ADHERENT.nomAdherent
, OEUVRE.titre
, EMPRUNT.dateEmprunt
, IF(EMPRUNT.dateRendu IS NULL,'',EMPRUNT.dateRendu) AS dateRendu
, EMPRUNT.noExemplaire
, IF(EMPRUNT.dateRendu IS NULL,DATEDIFF(CURDATE(),EMPRUNT.dateEmprunt),'') AS nbJour
FROM EMPRUNT
INNER JOIN ADHERENT ON EMPRUNT.idAdherent = ADHERENT.idAdherent
INNER JOIN EXEMPLAIRE ON EMPRUNT.noExemplaire = EXEMPLAIRE.noExemplaire
INNER JOIN OEUVRE ON EXEMPLAIRE.noOeuvre = OEUVRE.noOeuvre
WHERE EMPRUNT.idAdherent=3
ORDER BY EMPRUNT.dateEmprunt DESC;

-- 1.4 CONSULTER LES RETARDS
SELECT ADHERENT.idAdherent
, EMPRUNT.noExemplaire
, OEUVRE.titre
, ADHERENT.nomAdherent
, EMPRUNT.dateEmprunt
, EMPRUNT.dateRendu
, DATEDIFF( CURDATE(), EMPRUNT.dateEmprunt ) AS nbJoursEmprunt
, DATEDIFF( CURDATE(), DATE_ADD(EMPRUNT.dateEmprunt, INTERVAL 90 DAY) ) AS RETARD
, DATE_ADD(EMPRUNT.dateEmprunt, INTERVAL 90 DAY) AS dateRenduTheorique
, IF( CURRENT_DATE() > DATE_ADD(EMPRUNT.dateEmprunt, INTERVAL 90 DAY),1,0) AS flagRetard
, IF( CURRENT_DATE() > DATE_ADD(EMPRUNT.dateEmprunt, INTERVAL 120 DAY),1,0) AS flagPenalite
, IF( DATEDIFF( CURDATE(), EMPRUNT.dateEmprunt) > 120
    , IF( (DATEDIFF( CURDATE(), EMPRUNT.dateEmprunt)-120)*0.5 > 25
      , 25
      , (DATEDIFF( CURDATE(), EMPRUNT.dateEmprunt)-120)*0.5
      )
    , 0
  ) AS dette
FROM EMPRUNT
INNER JOIN ADHERENT ON EMPRUNT.idAdherent = ADHERENT.idAdherent
INNER JOIN EXEMPLAIRE ON EMPRUNT.noExemplaire = EXEMPLAIRE.noExemplaire
INNER JOIN OEUVRE ON EXEMPLAIRE.noOeuvre = OEUVRE.noOeuvre
INNER JOIN AUTEUR ON OEUVRE.idAuteur = AUTEUR.idAuteur
WHERE EMPRUNT.dateRendu IS NULL
    AND DATEDIFF( CURDATE(), DATE_ADD(EMPRUNT.dateEmprunt, INTERVAL 90 DAY) ) > 0
ORDER BY EMPRUNT.dateEmprunt;


-- 2 GESTION DES OEUVRES ET AUTEURS
-- 2.1 GESTION DES OEUVRES ET DE LEURS EXEMPLAIRES
-- 2.1.1 GESTION DES OEUVRES
-- 2.1.1.1 afficher toutes les oeuvres (nomAuteur, titreOeuvre, dateParution, nbTotal, nbDispo)
SELECT AUTEUR.nomAuteur
, OEUVRE.titre
, IFNULL(OEUVRE.dateParution,'') as dateParution
, COUNT(E1.noExemplaire) AS nbTotal
, COUNT(E2.noExemplaire) AS nbDispo
FROM OEUVRE
INNER JOIN AUTEUR ON OEUVRE.idAuteur = AUTEUR.idAuteur
LEFT JOIN EXEMPLAIRE E1 ON OEUVRE.noOeuvre = E1.noOeuvre
LEFT JOIN EXEMPLAIRE E2 ON E1.noExemplaire = E2.noExemplaire
  AND E2.noExemplaire NOT IN (
    SELECT EMPRUNT.noExemplaire
    FROM EMPRUNT
    WHERE EMPRUNT.dateRendu IS NULL)
GROUP BY OEUVRE.noOeuvre
ORDER BY AUTEUR.nomAuteur ASC, OEUVRE.titre ASC;

-- 2.1.1.2 ajouter une oeuvre
-- 2.1.1.2.1 afficher tous les auteurs (nom,idAuteur)
SELECT nomAuteur, idAuteur FROM AUTEUR ORDER BY nomAuteur;
-- 2.1.1.2.2 ajouter une oeuvre
INSERT INTO OEUVRE VALUES (NULL,'titre','0000-00-00',1);

-- 2.1.1.3 supprimer une oeuvre
DELETE FROM OEUVRE WHERE noOeuvre=1;

-- 2.1.1.4 modifier une oeuvre
-- 2.1.1.4.1 afficher tous les auteurs (nom,idAuteur)
SELECT nomAuteur, idAuteur FROM AUTEUR ORDER BY nomAuteur;
-- 2.1.1.4.2 modifier une oeuvre
UPDATE OEUVRE SET titre='titre', dateParution='0000-00-00' WHERE noOeuvre=1;

-- 2.1.2 GESTION DES EXEMPLAIRES
-- 2.1.2.1 afficher les exemplaires
-- 2.1.2.1.1 afficher informations oeuvre en cours (titre, auteur, dateParution)
SELECT OEUVRE.titre, AUTEUR.nomAuteur, OEUVRE.dateParution
FROM AUTEUR
INNER JOIN OEUVRE ON AUTEUR.idAuteur = OEUVRE.idAuteur
    AND OEUVRE.noOeuvre=1;

-- 2.1.2.1.2 afficher les nb d'exemplaires de l'oeuvre en cours (nbTotal, nbPresent)
SELECT COUNT(E1.noExemplaire) AS nbTotal
, COUNT(IF(E2.noExemplaire IS NULL,NULL,1)) AS nbPresent
FROM OEUVRE
LEFT JOIN EXEMPLAIRE E1 ON E1.noOeuvre = OEUVRE.noOeuvre
LEFT JOIN EXEMPLAIRE E2 ON E2.noExemplaire = E1.noExemplaire
  AND E2.noExemplaire NOT IN (
    SELECT EMPRUNT.noExemplaire
    FROM EMPRUNT
    WHERE EMPRUNT.dateRendu IS NULL
    )
WHERE OEUVRE.noOeuvre = 1;

-- 2.1.2.1.3 afficher tous les exemplaires de l'oeuvre en cours (noExemplaire, dispo, etat, dateAchat, prix)
SELECT E1.noExemplaire
, IF(E2.noExemplaire IS NULL,0,1) AS flagPresent
, E1.etat
, E1.dateAchat
, E1.prix
FROM OEUVRE
LEFT JOIN EXEMPLAIRE E1 ON E1.noOeuvre = OEUVRE.noOeuvre
LEFT JOIN EXEMPLAIRE E2 ON E2.noExemplaire = E1.noExemplaire
  AND E2.noExemplaire NOT IN (
    SELECT EMPRUNT.noExemplaire
    FROM EMPRUNT
    WHERE EMPRUNT.dateRendu IS NULL)
  WHERE OEUVRE.noOeuvre = 1
ORDER BY E1.noExemplaire ASC;

-- 2.1.2.2 ajouter un exemplaire
INSERT INTO EXEMPLAIRE VALUES (NULL,'neuf','0000-00-00',13.25,1);

-- 2.1.2.3 supprimer un exemplaire
DELETE FROM EXEMPLAIRE WHERE noExemplaire=1;

-- 2.1.2.4 modifier un exemplaire
UPDATE EXEMPLAIRE SET etat='neuf', dateAchat='0000-00-00', prix=12.31 WHERE noExemplaire=1;

-- 2.2 AFFICHER/EDITER/SUPPRIMER DES AUTEURS
-- 2.2.1 afficher tous les auteurs (nomAuteur, prenomAuteur, nbrOeuvre)
SELECT AUTEUR.idAuteur, AUTEUR.nomAuteur, AUTEUR.prenomAuteur, COUNT(OEUVRE.noOeuvre) AS nbOeuvre
FROM OEUVRE
RIGHT JOIN AUTEUR ON OEUVRE.idAuteur = AUTEUR.idAuteur
GROUP BY AUTEUR.nomAuteur
ORDER BY AUTEUR.nomAuteur, AUTEUR.prenomAuteur;

-- 2.2.1 ajouter un auteur
INSERT INTO AUTEUR VALUES (NULL,'nomAuteur','prenomAuteur');

-- 2.2.2 modifier un auteur
UPDATE AUTEUR SET nomAuteur='nomAuteur', prenomAuteur='prenomAuteur' WHERE idAuteur=1;

-- 2.2.3 afficher le nombre d'oeuvres d'un auteur
SELECT COUNT(OEUVRE.noOeuvre) AS nbOeuvre
FROM OEUVRE
RIGHT JOIN AUTEUR ON OEUVRE.idAuteur = AUTEUR.idAuteur AND AUTEUR.idAuteur=1;

-- 2.2.4 supprimer un auteur
DELETE FROM AUTEUR WHERE idAuteur=1;

-- 2.2.5 afficher les oeuvres d'un auteur
SELECT OEUVRE.titre FROM OEUVRE WHERE OEUVRE.idAuteur=1 ORDER BY OEUVRE.titre;

-- 3 GESTION DES ADHERENTS
-- 3.1 AFFICHER/EDITER/SUPPRIMER DES ADHERENTS
-- 3.1.1 afficher tous les adhérents (nom, adresse, datePaiement, nb emprunt en cours, statut paiement)
SELECT ADHERENT.idAdherent, ADHERENT.nomAdherent, ADHERENT.adresse, ADHERENT.datePaiement
, COUNT(EMPRUNT.idAdherent) as nbEmpruntEnCours
, IF(CURRENT_DATE()>=DATE_ADD(datePaiement, INTERVAL 11 MONTH) AND CURRENT_DATE()<DATE_ADD(datePaiement, INTERVAL 1 YEAR) ,1,0) AS flagRetardProche
, IF(CURRENT_DATE()>=DATE_ADD(datePaiement, INTERVAL 1 YEAR),1,0) AS flagRetard
, DATE_ADD(datePaiement, INTERVAL 1 YEAR) dateButoir
FROM ADHERENT
LEFT JOIN EMPRUNT ON ADHERENT.idAdherent = EMPRUNT.idAdherent
  AND EMPRUNT.dateRendu IS NULL
GROUP BY ADHERENT.idAdherent
ORDER BY ADHERENT.nomAdherent;

-- 3.1.2 ajouter un adhérent
INSERT INTO ADHERENT VALUES (NULL,'nomAdherent','adresse','0000-00-00');

-- 3.1.3 modifier un adhérent
UPDATE ADHERENT SET nomAdherent='nomAdherent', adresse='adresse', datePaiement='0000-00-00' WHERE idAdherent=1;

-- 3.1.4 supprimer un adhérent
DELETE FROM ADHERENT WHERE idAdherent=1;
