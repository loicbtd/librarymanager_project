# LibraryManager

L'application web LibraryManager vous permet de gérer votre bibliothèque, ses livres et ses usagers.

LibraryManager est aussi ma première application web sans framework qui a été réalisée dans le cadre de mon deuxième semestre de DUT Informatique.

Il s'agit donc là d'un devoir/preuve de concept de découverte de php et ne devrait pas être utilisé en production